import { getGreeting } from '../support/app.po';

describe('rosa', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to rosa!');
  });
});
