import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrimmerPipe } from './pipes/trimmer.pipe';
import { PhoneFormatterPipe } from './pipes/phone-formatter.pipe';



@NgModule({
  declarations: [
    TrimmerPipe,
    PhoneFormatterPipe,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TrimmerPipe,
    PhoneFormatterPipe,
  ]
})
export class SharedModule { }
