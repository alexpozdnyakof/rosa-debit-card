import { AbstractControl, ValidatorFn } from '@angular/forms';
import { filter } from 'rxjs/operators';

export function ValidateAddress(value: any): ValidatorFn {
    return (control: AbstractControl): {[key: string]: any} | null => {
        let isValid = true;
        value
        .pipe(filter((address) => address !== null))
        .subscribe(
            data => {
                isValid = validateField(data);
            }
        )
        return isValid ? null :  {'incorrect': true};
    };
  return null;
}

function validateField(value){
    return value.house !== undefined && value.house !== null;
}