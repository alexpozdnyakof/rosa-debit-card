import { AbstractControl } from '@angular/forms';

export function ValidateAge(control: AbstractControl) {
  if(control.value.length < 8) { return null};
  const rexp = /^\s*(3[01]|[12][0-9]|0?[1-9])(1[012]|0?[1-9])((?:19|20)\d{2})\s*$/;;
  const value = control.value
  if( control.value.match(rexp) == null){
     return { invalidDate: true }
  }
  const rexpValueMatch = control.value.match(rexp);
  const today = new Date();
  const day = Number(rexpValueMatch[1]);
  const month = Number(rexpValueMatch[2]);
  const year = Number(rexpValueMatch[3])
  const nowYear = Number(today.getFullYear());
  const date = new Date(year, month-1, day);
  if(day < 1 || day > 31) {
    return { invalidDate: true }
  }
  if(month < 1 || month > 12) {
    return { invalidDate: true }
  }
  if(day > 28 && month === 2){
    return new Date(year, 1, 29).getDate() === 29 ?
    null : {invalidDate: true}
  }
  if(year < 1902 || year > nowYear) {
    return { invalidDate: true }
  }
  if(date > today){
    return { invalidDate: true }
  }

  return null;
}