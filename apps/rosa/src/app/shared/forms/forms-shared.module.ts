import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxMaskModule } from 'ngx-mask'

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule
  ],
  declarations: [],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule
  ]
})
export class FormSharedModule { }
