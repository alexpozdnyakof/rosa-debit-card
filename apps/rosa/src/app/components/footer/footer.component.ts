import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';
import { ScrollService } from '../../services';

@Component({
  selector: 'rosa-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FooterComponent implements OnInit {

  constructor(
    private _scrollService: ScrollService,
  ) { }

  ngOnInit() {
  }
  public  scrollToTop(){
    this._scrollService.scrollTo();
  }

}
