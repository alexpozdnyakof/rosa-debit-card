import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'rosa-form',
  templateUrl: './form.component.html',
  styleUrls: ['./form.component.scss']
})
export class FormComponent implements OnInit {
  public rosaForm: FormGroup;
  public isDisabled = false;
  public control = new FormControl();
  public options = [
    { id: 1, label: 'One' },
    { id: 2, label: 'Two' },
    { id: 3, label: 'Three' }
  ];
  items = Array.from({ length: 10000 }).map((_, i) => ({
    id: i,
    label: `Item #${i}`
  }));

  change($event: any) {
  }

  constructor(private _fb: FormBuilder) {}
  ngOnInit() {
    this.initForm();
  }

  public initForm(): void {
    this.rosaForm = this._fb.group({
      name: [
        '',
        {
          validators: Validators.required,
          updateOn: 'blur'
        }
      ],
      work: [
        '',
      ],
      date: [
        '',
      ],
      type: ['', {
        validators: Validators.required,
        disabled: this.isDisabled,
        }, []],
      phone: ['9676125386',
        {
          validators: Validators.required,
          updateOn: 'blur',
          disabled: this.isDisabled }, []],
      segment: ['small', {
          validators: Validators.required,
          disabled: this.isDisabled,
      }, []],
      disclamer: ['', {
        disabled: this.isDisabled,
    }, []],
    });
  }
}
