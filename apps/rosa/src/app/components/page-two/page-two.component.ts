import { Component, OnInit } from '@angular/core';
import { GoogleAnalyticsService } from './../../services/google-analytics.service';

@Component({
  selector: 'rosa-page-two',
  templateUrl: './page-two.component.html',
  styleUrls: ['./page-two.component.css']
})
export class PageTwoComponent implements OnInit {

  constructor(private _googleAnalyticsService: GoogleAnalyticsService) { }

  ngOnInit() {
  }

  public sendLikeEvent() {
    //We call the event emmiter function from our service and pass in the details
    console.log('like event emitted');
    this._googleAnalyticsService.eventEmitter("cashloan", "GoalName", "userId", 1);
  }

}
