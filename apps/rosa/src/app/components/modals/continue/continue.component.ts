import { StateService } from './../../../services/state/state.service';
import { Component, OnInit } from '@angular/core';
import { ModalOverlayRef } from '../../../services/modal-ref';

@Component({
  selector: 'rosa-continue',
  templateUrl: './continue.component.html',
  styleUrls: ['./continue.component.scss']
})
export class ContinueComponent implements OnInit {

  constructor(
    private modalRef: ModalOverlayRef,
    private _stateService: StateService
  ) { }

  ngOnInit() {
  }
  public continue(){
    this._stateService.otpStatus$.next('CONTINUE')
    this._stateService.continue(true)
    this.modalRef.close();
  }
  public close(){
    this._stateService.continue(false)
    this.modalRef.close();
  }

}
