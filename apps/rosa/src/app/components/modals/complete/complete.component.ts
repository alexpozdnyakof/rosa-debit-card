import { Component, OnInit } from '@angular/core';
import { ModalOverlayRef } from '../../../services/modal-ref';

@Component({
  selector: 'rosa-complete',
  templateUrl: './complete.component.html',
  styleUrls: ['./complete.component.scss']
})
export class CompleteComponent implements OnInit {

  constructor(private modalRef: ModalOverlayRef) { }
  public toHomePage(){
    window.location.href = "https://rosbank.ru/";
  }
  ngOnInit() {
  }

}
