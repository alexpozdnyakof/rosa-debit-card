import { ScrollService } from './../../../services/scroll/scroll.service';
import { combineLatest, of, timer, BehaviorSubject } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { StateService } from './../../../services/state/state.service';
import { Component, OnInit, ContentChild, AfterViewInit, OnDestroy, AfterContentInit, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgControl } from '@angular/forms';
import { CodeContainerComponent } from 'libs/ui/src/lib/forms/code/components/code-container/code-container.component';
import { HttpService } from '../../../services/http.service';
import { ModalOverlayRef } from '../../../services/modal-ref';
import { switchMap, filter, tap, catchError, retry } from 'rxjs/operators';
export interface ControlData {
  value: string,
  isValid: boolean
}
@Component({
  selector: 'rosa-phone-code',
  templateUrl: './phone-code.component.html',
  styleUrls: ['./phone-code.component.scss']
})
export class PhoneCodeComponent implements OnInit, AfterViewInit, AfterContentInit, OnDestroy {
  public isDisabled = false;
  public codeForm: FormGroup;
  public phoneForm: FormGroup;

  public phoneNumber = this._stateService.phoneNumber$;
  public otpStatus = this._stateService.otpStatus$;
  public subscribeTimer: any;
  @ViewChild(CodeContainerComponent, { static: true })
  codeContainer: CodeContainerComponent;


  constructor(
    private _fb: FormBuilder,
    private modalRef: ModalOverlayRef,
    private _httpService: HttpService,
    private _stateService: StateService,
    private _scrollService: ScrollService
  ){
    this.sendOtp()
  }
  ngAfterContentInit(){}
  ngAfterViewInit(){}
  ngOnInit() {
    this.initForm();
    this.oberserableTimer()
  }
  ngOnDestroy(){
  }

  oberserableTimer() {
    timer(0, 1000).subscribe(val => {
      if(this.subscribeTimer > 0){
        this.subscribeTimer -= 1;
      }
    });
  }
  public initForm(): void {
    this.codeForm = this._fb.group({
      code: ['', {
        validators: [Validators.required],
        updateOn: 'change',
        disabled: this.isDisabled }, []],
    })
    this.controlValidation();
  }

  public controlValidation(){
    let type;

    combineLatest(
      [this.codeContainer.controlData$.pipe(
        filter((control: ControlData) => control.isValid)),
      this._stateService.otpStatus$]
    ).subscribe(
      data => console.log(data)
    )

    return combineLatest(
      [this.codeContainer.controlData$.pipe(
        filter((control: ControlData) => control.isValid)),
      this._stateService.otpStatus$]
    ).pipe(
      untilDestroyed(this),
      tap((config:Array<any>) => type = config[1]),
      tap((config:Array<any>) => console.log('config' + config)),
      switchMap((config:Array<any>) => this._httpService.check({otp:config[0].value , subject: config[1]}).pipe(
        catchError(err => of(err))
      )
      ),
      )
    .subscribe(
        (response: any) =>{
            if(response.status === 200){
              switch(type){
                case 'CONTINUE':
                    this.modalRef.close();
                    return this._stateService.continueOtp$.next(true);
                case 'CHECKOUT':
                    this.modalRef.close();
                    return this._stateService.applicationOtp$.next(true);
              }
            }
        },
        (res) => {
          catchError(res => of(`Bad Promise: ${res}`))
          if(res.status === 401){
            console.log('401')
            //this.codeForm.controls['code'].updateValueAndValidity({onlySelf: true, emitEvent: true});
            this.codeForm.controls['code'].setErrors({'incorrect': true});
          }
        }
    )
  }

  public changeNumber(){
    this._stateService.deliveryAgreement$.complete()
    this._stateService.applicationOtp$.complete();
    this.close();
  }
  public close(){

    this.modalRef.close();
  }
  public sendOtp(){
    this._stateService.applicationOtp$ = new BehaviorSubject(false);
    this.subscribeTimer = 30;
    return combineLatest(
      [this._stateService.phoneNumber$,
      this._stateService.otpStatus$]
    ).pipe(
      untilDestroyed(this),
      switchMap((config: Array<string>) => this._httpService.send({phone:config[0], subject: config[1]})))
    .subscribe(
      response => {
      if(response.status === 200){
        // show success message or timer
      }
      if(response.status === 401){
        // make error handling
        // 1 create and show error message at bottom of code
        // message like 'your otp is shit, try again'
        }
    }
    )
  }

}
