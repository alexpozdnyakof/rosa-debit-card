import { ModalOverlayRef } from './../../../services/modal-ref';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { StateService } from '../../../services/state/state.service';
import { DebitCardState } from '@monorepo/api-interfaces';

@Component({
  selector: 'rosa-disclaimer',
  templateUrl: './disclaimer.component.html',
  styleUrls: ['./disclaimer.component.scss']
})
export class DisclaimerComponent implements OnInit {
  public disclaimerForm: FormGroup;
  public state: DebitCardState;
  public initForm(): void {
    this.disclaimerForm = this._fb.group({
      adsFlag: [true, {validators: Validators.requiredTrue, updateOn: 'change'}],
      pdFlag: [true, {validators: Validators.requiredTrue, updateOn: 'change'}],
      tlcFlag: [true, {validators: Validators.requiredTrue, updateOn: 'change'}],
    })
  }
  public agree(){
    for(const c in this.disclaimerForm.controls){
      if(this.disclaimerForm.controls[c]){
        this.disclaimerForm.controls[c].markAsTouched();
        this.disclaimerForm.controls[c].updateValueAndValidity();
      }
    }
    if(!this.disclaimerForm.valid) return;
    this._stateService.presendAgreement$.next(true);
    this.modalRef.close();
  }
  constructor(
    private _fb: FormBuilder,
    private modalRef: ModalOverlayRef,
    private _stateService: StateService,) { }

  ngOnInit() {
    this.initForm()
    this._stateService.getState$().subscribe(
      state => this.state = state
    )
  }


}
