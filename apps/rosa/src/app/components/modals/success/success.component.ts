import { StateService } from './../../../services/state/state.service';
import { ModalOverlayRef } from './../../../services/modal-ref';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'rosa-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {
  public office$ = this._stateService.office$;
  public phone$ = this._stateService.phoneNumber$;
  constructor(
    private modalRef: ModalOverlayRef,
    private _stateService: StateService,
  ) { }

  ngOnInit() {}
  public redirect(){
    window.location.href = "https://store.rosbank.ru/crossale/";
  }
  public close(){
    this.modalRef.close();
  }

}
