import { DebitCardState } from '@monorepo/api-interfaces';
import { StateService } from './../../../services/state/state.service';
import { Component, OnInit } from '@angular/core';
import { ModalOverlayRef } from '../../../services/modal-ref';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'rosa-agreement',
  templateUrl: './agreement.component.html',
  styleUrls: ['./agreement.component.scss']
})
export class AgreementComponent implements OnInit {
  public state: DebitCardState;
  constructor(
    private modalRef: ModalOverlayRef,
    private _stateService: StateService,) { }
  ngOnInit() {
    this._stateService.getState$().subscribe(
      state => this.state = state
    )
  }
  public agree(){
    this._stateService.deliveryAgreement$.next(true)
    this.modalRef.close();
  }

  public close(){
    this.modalRef.close();
  }

}
