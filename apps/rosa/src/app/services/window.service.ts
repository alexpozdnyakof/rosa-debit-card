import { Injectable, Inject, OnInit, Injector, ComponentRef } from '@angular/core';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';
import { WindowRef } from './window.ref';
interface WindowConfig {
  panelClass?: string;
  hasBackdrop?: boolean;
  backdropClass?: string;
  closeByBackdrop?: boolean;
  data?: any;
  //content: PopoverContent;
}
const DEFAULT_CONFIG: WindowConfig = {
  hasBackdrop: true,
  backdropClass: 'dark-backdrop',
  panelClass: 'tm-file-preview-dialog-panel',
  closeByBackdrop: true,
}

@Injectable({
  providedIn: 'root'
})
export class WindowService {

  constructor(private overlay: Overlay, private injector: Injector) { }


  public open<T>(config: WindowConfig = {}){
    const windowConfig = { ...DEFAULT_CONFIG, ...config };
    const overlayConfig = this.getOverlayConfig({ ...DEFAULT_CONFIG, ...config });
    const overlayRef = this.createOverlay(windowConfig);
   // const windowRef = new WindowRef<T>(overlayRef, content, data);

/*
    const overlayRef = this.overlay.create(this.getOverlayConfig({ origin, width, height }));
    const popoverRef = new PopoverRef<T>(overlayRef, content, data);

    const injector = this.createInjector(popoverRef, this.injector);
    overlayRef.attach(new ComponentPortal(PopoverComponent, null, injector));

    return popoverRef;
    */
  }
  private createOverlay(config: WindowConfig) {
    const overlayConfig = this.getOverlayConfig(config);
    return this.overlay.create(overlayConfig);
  }
  private getOverlayConfig(config: WindowConfig): OverlayConfig {
    const positionStrategy = this.overlay.position()
      .global()
      .centerHorizontally()
      .centerVertically();

    const overlayConfig = new OverlayConfig({
      hasBackdrop: config.hasBackdrop,
      backdropClass: config.backdropClass,
      panelClass: config.panelClass,
      scrollStrategy: this.overlay.scrollStrategies.block(),
      positionStrategy
    });

    return overlayConfig;
  }
}
