import { Injectable, isDevMode } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';


@Injectable()
export class CookieInterceptor implements HttpInterceptor {
    constructor() {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        const url = request.url;
        if(url.indexOf('dadata') !== -1){return next.handle(request);}
        if(url.indexOf('api.rosbank.ru') !== -1){return next.handle(request);}
        if(url.indexOf('localhost') !== -1){return next.handle(request);}
        if(url.indexOf('offices') !== -1){return next.handle(request);}
        //exception for card info api
        request = request.clone({
            withCredentials: true,
            headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
        });
        return next.handle(request);
    }
}




