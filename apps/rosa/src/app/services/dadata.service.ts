import { Injectable, isDevMode } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DadataService {

  constructor(
    private _http: HttpClient,
  ) { }
  private _apiUrl: string;
  private _apiKey = '5c0fa69268870b7061361c5bdfc30bc1b1e95846';
  public search(query: string, type: 'address' | 'party' | 'fio' | 'fms_unit' | 'fns_unit' ) {
    let _headers;
    if(isDevMode()){
      this._apiUrl = `https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/${type}`;
      _headers = new HttpHeaders({ 'Content-type': 'application/json; charset = UTF-8', 'Accept': 'application/json', 'Authorization': `Token ${this._apiKey}` });
    } else {
      this._apiUrl = `https://store.rosbank.ru/dadata-suggestions/${type}`;
      _headers = new HttpHeaders({ 'Content-type': 'application/json'});
    }
    const queryData = JSON.stringify({
      'query': query,
      'count': '5'
    });
    return this._http.post(this._apiUrl, queryData, { headers: _headers }).pipe(
      map((data: any) => data.suggestions)
    )
  }
  // search in modules disabled in inner dadata service
  public searchCloud(query: string, type: 'fms_unit' ) {
    this._apiUrl = `https://dadata.ru/api/v2/suggest/${type}`;
    const apiKey = 'edce62a5a22e91ea4e0e1abb1718f2250a86e8e3';
    const _headers = new HttpHeaders({ 'Content-type': 'application/json; charset = UTF-8', 'Accept': 'application/json', 'Authorization': `Token ${apiKey}` });
    const queryData = JSON.stringify({
      'query': query,
      'count': '5'
    });
    return this._http.post(this._apiUrl, queryData, { headers: _headers }).pipe(
      map((data: any) => data.suggestions)
    )
  }
}
