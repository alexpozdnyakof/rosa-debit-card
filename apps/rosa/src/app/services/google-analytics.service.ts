import { Injectable } from '@angular/core';

declare let ga: Function; // Declare ga as a function

@Injectable({
  providedIn: 'root'
})
export class GoogleAnalyticsService {

  constructor() { }


  //create our event emitter to send our data to Google Analytics
   public eventEmitter(
     eventCategory: string,
     eventAction: string,
     eventLabel: string = null,
     eventValue: number = null) {
       console.log('service send');
        ga('send',  {
          hitType: 'event',
          eventCategory: eventCategory,
          eventLabel: eventLabel,
          eventAction: eventAction,
          eventValue: eventValue
        })
      }

}