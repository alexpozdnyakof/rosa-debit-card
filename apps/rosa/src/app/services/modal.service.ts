import { CompleteComponent } from './../components/modals/complete/complete.component';
import { Injectable, Inject, OnInit, Injector, ComponentRef } from '@angular/core';
import { Overlay, OverlayConfig, OverlayRef } from '@angular/cdk/overlay';
import { ComponentPortal, PortalInjector } from '@angular/cdk/portal';
import { ModalOverlayRef } from './modal-ref';
import { AgreementComponent } from '../components/modals/agreement/agreement.component';
import { PhoneCodeComponent } from '../components/modals/phone-code/phone-code.component';
import { BehaviorSubject } from 'rxjs';
import { ContinueComponent } from '../components/modals/continue/continue.component';
import { SuccessComponent } from '../components/modals/success/success.component';
import { DisclaimerComponent } from '../components/modals/disclaimer/disclaimer.component';

export interface Image {
  name: string;
  url: string;
}

interface ModalConfig {
  panelClass?: string;
  hasBackdrop?: boolean;
  backdropClass?: string;
  closeByBackdrop?: boolean;
  type?: "phone" | "agreements" | "continue" | "success" | "completed" | "disclaimers";
}

const DEFAULT_CONFIG: ModalConfig = {
  hasBackdrop: true,
  backdropClass: 'dark-backdrop',
  panelClass: 'tm-file-preview-dialog-panel',
  closeByBackdrop: true,
  type: "agreements"
}


@Injectable({
  providedIn: 'root'
})
export class ModalService {
  public agreeSuccess$: BehaviorSubject<Boolean> = new BehaviorSubject(false);
  constructor(
    private injector: Injector,
    private overlay: Overlay,
  ) {
  }

  open(config: ModalConfig = {}) {
    // Override default configuration
    const dialogConfig = { ...DEFAULT_CONFIG, ...config };
    // Returns an OverlayRef which is a PortalHost
    const overlayRef = this.createOverlay(dialogConfig);
    // Instantiate remote control
    const dialogRef = new ModalOverlayRef(overlayRef);
    const overlayComponent = this.attachDialogContainer(overlayRef, dialogConfig, dialogRef);
    if(dialogConfig.closeByBackdrop){
      overlayRef.backdropClick().subscribe(_ => dialogRef.close());
    }
    return dialogRef;
  }


  private createOverlay(config: ModalConfig) {
    const overlayConfig = this.getOverlayConfig(config);
    return this.overlay.create(overlayConfig);
  }

  // refactor it 🙀
  private attachDialogContainer(overlayRef: OverlayRef, config: ModalConfig, dialogRef: ModalOverlayRef) {
    const injector = this.createInjector(config, dialogRef);
    if(config.type === "phone" ){
      const containerPhonePortal = new ComponentPortal(PhoneCodeComponent, null, injector);
      const containerPhoneRef: ComponentRef<PhoneCodeComponent> = overlayRef.attach(containerPhonePortal);
      return containerPhoneRef.instance;
    }
    if(config.type === "continue" ){
      const containerContinuePortal = new ComponentPortal(ContinueComponent, null, injector);
      const containerContinueRef: ComponentRef<ContinueComponent> = overlayRef.attach(containerContinuePortal);
      return containerContinueRef.instance;
    }
    if(config.type === "success" ){
      const containerSuccessPortal = new ComponentPortal(SuccessComponent, null, injector);
      const containerSuccessRef: ComponentRef<SuccessComponent> = overlayRef.attach(containerSuccessPortal);
      return containerSuccessRef.instance;
    }
    if(config.type === "completed" ){
      const containerCompletePortal = new ComponentPortal(CompleteComponent, null, injector);
      const containerCompleteRef: ComponentRef<CompleteComponent> = overlayRef.attach(containerCompletePortal);
      return containerCompleteRef.instance;
    }
    if(config.type === "disclaimers" ){
      const containerDisclaimerPortal = new ComponentPortal(DisclaimerComponent, null, injector);
      const containerDisclaimerRef: ComponentRef<DisclaimerComponent> = overlayRef.attach(containerDisclaimerPortal);
      return containerDisclaimerRef.instance;
    }

    const containerAgreementPortal = new ComponentPortal(AgreementComponent, null, injector);
    const containerAgreementRef: ComponentRef<AgreementComponent> = overlayRef.attach(containerAgreementPortal);
    return containerAgreementRef.instance;
  }

  private createInjector(config: ModalConfig, dialogRef: ModalOverlayRef): PortalInjector {
    const injectionTokens = new WeakMap();

    injectionTokens.set(ModalOverlayRef, dialogRef);

    return new PortalInjector(this.injector, injectionTokens);
  }

  private getOverlayConfig(config: ModalConfig): OverlayConfig {
    const positionStrategy = this.overlay.position()
      .global()
      .centerHorizontally()
      .centerVertically();

    const overlayConfig = new OverlayConfig({
      hasBackdrop: config.hasBackdrop,
      backdropClass: config.backdropClass,
      panelClass: config.panelClass,
      scrollStrategy: this.overlay.scrollStrategies.block(),
      positionStrategy
    });

    return overlayConfig;
  }



}
