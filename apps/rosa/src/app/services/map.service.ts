import { HttpService } from './http.service';
import { StateService } from './state/state.service';
import { Injectable, ChangeDetectorRef, NgZone } from '@angular/core';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, pluck } from 'rxjs/operators';
import { Office } from '@monorepo/api-interfaces';
declare global {
  const ymaps;
}
@Injectable({
  providedIn: 'root'
})
export class MapService {
  public map$: BehaviorSubject<any> = new BehaviorSubject<any>(null);
  private map: any;
  public objectManager: any;
  public objectManager$$: BehaviorSubject<any> = new BehaviorSubject(null);
  private _windowWidth = window.innerWidth;

  constructor(
    private _stateService: StateService,
    private _httpService: HttpService,
    public zone: NgZone
  ) {}

  getMap(element: string, center: number[]): Observable<any> {
    const defaultBehavior: string[] = [
      'scrollZoom',
      'drag',
      'dblClickZoom',
      'rightMouseButtonMagnifier'
    ];
    const scriptYmaps = document.createElement('script');
    scriptYmaps.src =
      'https://api-maps.yandex.ru/2.1/?apikey=61d55833-e88c-4afd-be02-52387bbf6d1d&lang=ru_RU';
    document.body.appendChild(scriptYmaps);
    const createMap = () => {
      this.map = new ymaps.Map(
        element,
        {
          behaviors: ['multiTouch'],
          center: center,
          zoom: 9,
          controls: [],
          type: 'yandex#map'
        },
        {
          autoFitToViewport: 'always',
          suppressMapOpenBlock: true
        }
      );
      this.map.behaviors.disable(defaultBehavior);
      if (this._windowWidth < 768) {
        this.map.behaviors.disable('drag');
        //this.map.behaviors.enable('multiTouch');
        //disable drag enable multitouch
      } else {
        this.map.behaviors.enable('drag');
        //enabledrag
      }
      this.addObjects();
      // detect user geolocation and centring map

      ymaps.geolocation
        .get({
          mapStateAutoApply: true
        })
        .then(result => {
          //this.map.panTo(result.geoObjects.position, {flying: true});
          this.map.setCenter(result.geoObjects.position, 13);
        });
    };
    scriptYmaps.onload = () => {
      ymaps.ready(createMap);
    };
    this.map$.next(true);
    return this.map$.asObservable();
  }
  setCenter(coords: Array<any>) {
    this.map.panTo(coords, { flying: true });
  }
  calculateRoute(from, to) {
    ymaps
      .route([from, to], { mapStateAutoApply: false })
      .done(function(router) {
        console.log(router.getLength());
      });
  }

  addObjects() {
    // setup yamaps
    this.objectManager = new ymaps.ObjectManager({
      clusterize: true,
      gridSize: 64,
      // geoObjectOpenBalloonOnClick: true,
      preset: 'https://rosbank.ru/static/images/map-cluster.svg'
    });
    const clusterIcons = [
      {
        href: 'https://rosbank.ru/static/images/map-cluster.svg',
        size: [60, 60],
        offset: [-30, -30]
      },
      {
        href: 'https://rosbank.ru/static/images/map-cluster.svg',
        size: [60, 60],
        offset: [-30, -30]
      }
    ];
    this.map.geoObjects.add(this.objectManager);
    this.objectManager.objects.options.set(
      'iconImageHref',
      'https://rosbank.ru/static/images/map-pin.svg'
    );
    this.objectManager.objects.options.set('iconLayout', 'default#image');
    this.objectManager.objects.options.set('iconImageSize', [50, 64]);
    const MyIconContentLayout = ymaps.templateLayoutFactory.createClass(
      `<div style="color: #FFFFFF; font-family: Muller; line-height: 60px; font-size: 24px; text-align: center; letter-spacing: 0.02em;">$[properties.geoObjects.length]</div>`
    );
    const clusterNumbers = [2];
    this.objectManager.clusters.options.set({
      // Если опции для кластеров задаются через кластеризатор,
      // необходимо указывать их с префиксами "cluster".
      clusterIcons: clusterIcons,
      clusterNumbers: clusterNumbers,
      clusterIconContentLayout: MyIconContentLayout
    });
    //this.objectManager.clusters.options.set('preset', 'https://rosbank.ru/static/images/map-cluster.svg');
    this.objectManager.clusters.setClusterOptions({
      clusterIcons: clusterIcons,
      preset: 'default#image'
    });
    // get office list from backend and convert to yamaps feature collection
    this._httpService
      .office()
      .pipe(
        pluck('elements'),
        map((offices: Office[]) => {
          return this.converToFeaturesList(offices);
        })
      )
      .subscribe(
        (data: any) => {
          this.objectManager.add(data);
        },
        (err: any) => {
          throw new Error('Не удалось загрузить карты');
        }
      );
    // add handlers to map objects
    this.objectManager.objects.events.add(['click'], e => {
      console.log(e.get('coords'));
      //this.objectManager.objects.setObjectOptions(this.activeOffice, {iconImageHref: 'https://rosbank.ru/static/images/map-pin-active.svg'}); // set active image
      this.map.panTo(e.get('coords'), { flying: true }); // set office in center
      const officeInfo = this.objectManager.objects.getById(e.get('objectId')); // get active office
      this._stateService.visitInfo$.next(officeInfo.properties.address); // store office address in service to show after complete
      const office = this.objectManager.objects.getById(e.get('objectId')); // get office info from map
      console.log(office);
      this._stateService.office$.next(office.properties); // store office info to state service
      this.toggleOffice(true); // show baloon
    });
  }
  // control office state
  public toggleOffice(isOffice: boolean) {
    if (isOffice) {
      return this.map.behaviors.disable('drag');
    }
    if (this._windowWidth > 768) {
      this.map.behaviors.enable('drag');
    }
    return this._stateService.office$.next(null);
  }

  // convert data from api to yamaps collection
  converToFeaturesList(offices: Office[]) {
    let iterator = 0;
    const filtered = offices.filter(office => office.code);
    const features = {
      type: 'FeatureCollection',
      features: filtered.map(office => {
        return {
          type: 'Feature',
          id: iterator++,
          geometry: { type: 'Point', coordinates: office.coords },
          properties: {
            name: office.name,
            address: office.address,
            schedule: office.schedule,
            code: office.code,
            misc: office.misc,
            metro: office.metro
          }
        };
      })
    };
    return JSON.stringify(features);
  }

  public changeZoom(zoomDirection: 'in' | 'out') {
    return zoomDirection === 'in'
      ? this.map.setZoom(this.map.getZoom() + 1, {
          checkZoomRange: true,
          smooth: true
        })
      : this.map.setZoom(this.map.getZoom() - 1, {
          checkZoomRange: true,
          smooth: true
        });
  }
}
