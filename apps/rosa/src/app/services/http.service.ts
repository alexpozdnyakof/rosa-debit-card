import { OtpRequestConfig, OtpResponseConfig } from '@monorepo/api-interfaces';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { BehaviorSubject, Observable } from 'rxjs';
import { DebitCardState, Card, OfficeList, Otp } from '@monorepo/api-interfaces'


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  public state: DebitCardState;

  public setphone$: BehaviorSubject<any> = new BehaviorSubject('');


  constructor(private _http: HttpClient) { }
  start(){
    const _headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this._http.get(`${environment.api.endpoint}/start`,  {headers: _headers})
  }

  parsed(){
    return this._http.get(`${environment.api.parsed}`)
  }
  save(formState: DebitCardState){
    return this._http.post(`${environment.api.endpoint}/save`, formState)
  }
  checkout(formState: DebitCardState){
    return this._http.post(`${environment.api.endpoint}/checkout`, formState)
  }
  // send request sms code
  send(config: OtpRequestConfig){
    return this._http.post<Otp>(`${environment.api.endpoint}/send`, config, {observe: 'response'})
  }
  // check is sms code valid
  check(config: OtpResponseConfig){
    return this._http.post(`${environment.api.endpoint}/check`, config,  {observe: 'response'})
  }
  continue(state: DebitCardState): Observable<DebitCardState>{
    return this._http.post<DebitCardState>(`${environment.api.endpoint}/continue`, state)
  }
  create(state: DebitCardState): Observable<DebitCardState>{
    return this._http.post<DebitCardState>(`${environment.api.endpoint}/create`, state)
  }
  // search previous user requests
  search(state: DebitCardState): Observable<DebitCardState>{
    return this._http.post<DebitCardState>(`${environment.api.endpoint}/search`, state)
  }
  card(slug: string): Observable<Card>{
    return this._http.get<Card>(`${environment.api.card}/${slug}`)
  }
  // get offices list
  office(): Observable<OfficeList>{
    const _headers = new HttpHeaders({ 'Content-Type': 'application/json' });
    return this._http.post<OfficeList>(`/rest/offices`,  {headers: _headers});
  }
}
