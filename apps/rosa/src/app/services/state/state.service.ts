import { DebitCardState, AddressValue, NameValue, Office } from '@monorepo/api-interfaces';
import { HttpService } from './../http.service';
import { Injectable, Inject } from '@angular/core';
import { of, BehaviorSubject, Observable, Subject } from 'rxjs';
import { mapValues, omit, pickBy, negate }  from 'lodash';

export interface Visit{
  branch: string;
  date: string;
}
@Injectable({
  providedIn: 'root'
})
export class StateService {
  private _state$: BehaviorSubject<DebitCardState> = new BehaviorSubject(null);
  private _modalsState = {
    continue: false,
    continueOtp: false,
    applicationOtp: false,
    deliveryAgreement: false
  }

  office$: BehaviorSubject<Office> = new BehaviorSubject(null);

  public serviceState: DebitCardState;
  public otpStatus$: BehaviorSubject<string> = new BehaviorSubject('');
  public phoneNumber$: BehaviorSubject<string> = new BehaviorSubject('');
  public visitInfo$: BehaviorSubject<string> = new BehaviorSubject(null);
  public continue$: BehaviorSubject<boolean> = new BehaviorSubject(null);
  public continueOtp$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public applicationOtp$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public deliveryAgreement$: BehaviorSubject<boolean> = new BehaviorSubject(false);
  public presendAgreement$: BehaviorSubject<boolean> = new BehaviorSubject(false);

  constructor(
    private _httpService: HttpService,) {
  }

  public getModalState(){
    return of(this._modalsState)
  }
  public continue(changes){
      this.continue$.next(changes);
      this._modalsState.continue = changes
  }
  public continueOtp(changes){
    if(changes){
      this.continueOtp$.next(true);
      this._modalsState.continueOtp = true
    }
  }
  public applicationOtp(changes){
    if(changes){
      this.applicationOtp$.next(true)
      this._modalsState.applicationOtp = true
    }
  }
  public deliveryAgreement(changes){
    if(changes){
      this.deliveryAgreement$.next(true)
      this._modalsState.deliveryAgreement = true
    }
  }

  public getState$(){
    return this._state$.asObservable()
  }
  public setState$(state){
    return this._state$.next(state);
  }

  public getTimeZone(): string {
    return new Date().toTimeString().slice(9,17);
  }
  public parseToFormValues(formState){
    const fieldsToExclude = ['factAddress', 'regAddress', 'fio', 'mobilePhone', 'birthDate', 'docIssueDate'];
    let formValues = omit(formState.forms.application.fields, fieldsToExclude);
    // restruct form init to key: value pair
    formValues = mapValues(formValues,
      function(f) {
        return f.value
      });
    // filter form initial values
    function isExist(v) {
      return v == null && v == undefined
    }
    formValues = pickBy(formValues, negate(isExist))
    return formValues;
  }

  public cleanObj(obj: Object, fieldsToExclude?: Array<string> ){
    let formValues = omit(obj, fieldsToExclude);
    // filter form initial values
    function isExist(v) {
      return v == null && v == undefined
    }
    formValues = pickBy(formValues, negate(isExist))
    return formValues;
  }


  public patchBranchFormState(state, form){
    state.forms.checkoutBranch.fields.visitBranch.value = form.visitBranch
    state.forms.checkoutBranch.fields.visitDate.value = this.formatDate(form.visitDate)
    this._state$.next(state);
    return state;
  }
  public patchDeliveryFormState(state, form){
    state.forms.checkoutDelivery.fields.codeWord.value = form.codeWord
    state.forms.checkoutDelivery.fields.smsInfo.value = form.smsInfo
    state.forms.checkoutDelivery.fields.safeAcc.value = form.safeAcc
    state.forms.checkoutDelivery.fields.thirdPartyAbs.value = form.thirdPartyAbs
    this._state$.next(state);
    return state;
  }
  public patchFormState(state, form){
    state.forms.application.fields.mobilePhone.value = this.convertPhone(form.mobilePhone);
    state.forms.application.fields.birthDate.value = this.formatDate(form.birthDate);
    state.forms.application.fields.birthPlace.value = form.birthPlace;
    state.forms.application.fields.docEntityCode.value = form.docEntityCode;
    state.forms.application.fields.docIssueDate.value = this.formatDate(form.docIssueDate);
    state.forms.application.fields.docIssueEntity.value = form.docIssueEntity;
    state.forms.application.fields.docSerNum.value = form.docSerNum;
    state.forms.application.fields.email.value = form.email;
    state.forms.application.fields.fio.value = {
        lastName: form.lastName,
        firstName: form.firstName,
        middleName: form.middleName,
        gender: form.gender,
        stringValue: `${form.lastName} ${form.firstName} ${form.middleName}`
    }
    state.forms.application.fields.factAddressEquality.value = form.factAddressEquality;
    state.forms.application.fields.userTimezone.value = this.getTimeZone();
    state.forms.application.fields.adsFlag.value = form.adsFlag;
    state.forms.application.fields.pdFlag.value = form.pdFlag;
    state.forms.application.fields.tlcFlag.value = form.tlcFlag;
    if(form.factAddressEquality){
      state.forms.application.fields.factAddress.value = state.forms.application.fields.regAddress.value
    }
    this._state$.next(state);
    return state;
  }

  public isPassportExpired(passYear: number, userYear: number){
    if(!passYear || !userYear ) return true;
    const today = new Date();
    const year = Number(today.getFullYear())
    const userAges = year - userYear
    const yearPoints = {
      years14: userYear  + 14,
      years20: userYear  + 20,
      years45: userYear  + 45,
    }
    if(userAges >= 14 && userAges < 20){
      return passYear >= yearPoints.years14 && passYear < yearPoints.years20;
    }
    if(userAges >= 20 && userAges < 45){
      return passYear >= yearPoints.years20 && passYear < yearPoints.years45;
    }
    if(userAges >= 45){
      return passYear >= yearPoints.years45;
    }
    return false;
  }

  public makeAddressFromDadata(address): AddressValue{
    return {
      stringValue: address.value,
      country: address.data.country,
      postalCode: address.data.postal_code,
      region: address.data.region,
      districtType: address.data.area_type,
      district: address.data.area,
      cityType: address.data.city_type,
      city: address.data.city,
      settlementType: address.data.settlement_type,
      settlement: address.data.settlement,
      streetType: address.data.street_type,
      street: address.data.street,
      house: address.data.house,
      building: address.data.block,
      apartment: address.data.flat
    }
  }
  public makeNameFromDadata(name): NameValue{
    return {
      stringValue: name.data.value,
      lastName: name.data.surname,
      firstName: name.data.name,
      middleName: name.data.patronymic,
      gender: name.data.gender
    }
  }
  public isNullOrUndefined(value): boolean{
      return typeof value === "undefined" || value === null;
  }
  public convertPhone(phone): string {
      if(this.isNullOrUndefined(phone)) return;
      if(phone.indexOf('+7') !== 0){return '+7' + phone}
      return phone.replace('+7', '')
  }
  public formatDate(date: string, type: 'fromSiebel' | 'toSiebel' = 'toSiebel'): string {
    return type === 'toSiebel' ? `${date.slice(4,8)}-${date.slice(2,4)}-${date.slice(0,2)}`:
    `${date.slice(8,10)}${date.slice(5,7)}${date.slice(0,4)}`;
  }

}

