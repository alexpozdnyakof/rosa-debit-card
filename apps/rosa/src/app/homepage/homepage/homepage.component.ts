import {
  Component,
  OnInit,
  AfterViewInit,
  Renderer2,
  OnDestroy,
  isDevMode,
  ViewChild,
  HostListener
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {
  distinctUntilChanged,
  filter,
  debounceTime,
  switchMap,
  tap
} from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import {
  DadataService,
  HttpService,
  ModalService,
  StateService,
  CookieService
} from '../../services';
import { Router, ActivatedRoute } from '@angular/router';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { DebitCardState, Card } from '@monorepo/api-interfaces';
import {
  ValidateAddress,
  ValidateSpaces,
  ValidateAge
} from '../../shared/validators';
import { environment } from './../../../environments/environment';
import { RosaButtonDirective } from 'libs/ui/src/lib/buttons/rosa-button.directive';

@Component({
  selector: 'rosa-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit, AfterViewInit, OnDestroy {
  public isLoading = false;
  public rosaForm: FormGroup;
  public isDisabled = false;
  public isReadonly = false;
  public registryPlace$: Observable<any>;
  public factPlace$: Observable<any>;
  public fms$: Observable<any>;
  public localState: DebitCardState;
  public searchState: DebitCardState;
  public userYear: number;
  public passYear: number;
  public isPhoneChanged = false;
  public regAddressValue: BehaviorSubject<any> = new BehaviorSubject(null);
  public factAddressValue: BehaviorSubject<any> = new BehaviorSubject(null);
  public esiaTooltipText =
    'Ускорить заполнение данных можно через авторизацию в сервисе Госуслуги. Процедура авторизации защищена и полностью безопасна.';
  public errorMessages = {
    docIssueDate: `укажите дату выдачи паспорта`
  };
  public esialink = `${environment.esia}?returnUrl=${window.location.href}`;
  public deliveryAvailable: boolean;
  public card$: Observable<Card>;
  @ViewChild(RosaButtonDirective, { static: true })
  sendButton: RosaButtonDirective;

  constructor(
    private _fb: FormBuilder,
    private _httpService: HttpService,
    private _dadataService: DadataService,
    private previewDialog: ModalService,
    private _stateService: StateService,
    private _cookieService: CookieService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngAfterViewInit() {}

  ngOnInit() {
    this.card$ = this.activatedRoute.queryParams.pipe(
      untilDestroyed(this),
      filter(params => params['card']),
      switchMap(params => this._httpService.card(params['card']))
    );
    // 📡 get form config
    this._httpService
      .start()
      .pipe(tap((state: DebitCardState) => this._stateService.setState$(state)))
      .subscribe((state: DebitCardState) => {
        this.localState = state;
        this.patchForm(this.localState);
        this.addProductParams();
        this.isReadonly = state.esiaAuth && state.esiaAuth.authorized;
      });
    this.initForm();
    // 🎙create listeners
    this.fms$ = this.dadataCloudListener('docIssueEntity', 'fms_unit');
    this.registryPlace$ = this.dadataListener('regAddress', 'address');
    this.factPlace$ = this.dadataListener('factAddress', 'address');
    this.getModalConfig();
    this.subscribeToFormValues();
  }

  public esiaAuth() {
    const storageData = Object.assign(
      { mobilePhone: `+7${this.rosaForm.get('mobilePhone').value}` },
      { email: this.rosaForm.get('email').value }
    );
    localStorage.setItem('contacts', JSON.stringify(storageData));
    return window.location.replace(this.esialink);
  }

  public addProductParams() {
    this.localState.cardCategory = 'Visa (неименная) #МожноВСЁ';
  }

  public patchForm(formState) {
    if (formState.prefilled && this.getContactsFromStorage()) {
      const contacts = this.getContactsFromStorage();
      // refactor it
      formState.forms.application.fields.mobilePhone.value =
        contacts.mobilePhone;
      formState.forms.application.fields.email.value = contacts.email;
      localStorage.removeItem('contacts');
    }
    const applicationFields = formState.forms.application.fields;
    this.parseName(applicationFields.fio.value);
    this.parseAddress(applicationFields.regAddress.value, 'reg');
    this.parseAddress(applicationFields.factAddress.value, 'fact');
    this.setPhone(applicationFields.mobilePhone.value);
    this._setDate(applicationFields.birthDate.value, 'birthDate');
    this._setDate(applicationFields.docIssueDate.value, 'docIssueDate');
    const formValues = this._stateService.parseToFormValues(formState);
    this.rosaForm.patchValue(formValues, { onlySelf: true, emitEvent: false });
  }

  getContactsFromStorage() {
    const contacts = localStorage.getItem('contacts');
    return contacts ? JSON.parse(contacts) : null;
  }

  subscribeToFormValues() {
    for (const key of Object.keys(this.rosaForm.controls)) {
      const subscription = this.rosaForm.controls[key].valueChanges
        .pipe(
          untilDestroyed(this),
          distinctUntilChanged()
        )
        .subscribe(value => {
          switch (key) {
            case 'mobilePhone':
              return value.length === 10
                ? this.searchPrev(this._stateService.convertPhone(value))
                : null;
            case 'factAddressEquality':
              return this.handleAddressEquality(value);
            case 'birthDate':
              if (value.length === 8) {
                return this._setBirthDate(value);
              }
            // tslint:disable-next-line: no-switch-case-fall-through
            case 'docIssueDate':
              if (value.length === 8) {
                return this._setDocIssueDate(value);
              }
          }
        });
    }
  }

  // searchPreviousRequest
  public searchPrev(phoneValue: string) {
    this._stateService.phoneNumber$.next(phoneValue);
    this.localState.forms.application.fields.mobilePhone.value = phoneValue;
    this._httpService
      .search(this.localState)
      .pipe(
        tap(
          (state: DebitCardState) =>
            (this.localState.applicationID = state.applicationID)
        ),
        tap((state: DebitCardState) => {
          this.deliveryAvailable = state.deliveryAvailable;
        })
      )
      .subscribe(state => {
        if (state.esiaAuth) {
          return;
        }
        switch (state.status) {
          case 'FOUND':
            this.searchState = state;
            return this.previewDialog.open({
              closeByBackdrop: false,
              type: 'continue'
            });
          case 'IN_EXECUTION':
            this.searchState = state;
            return this.previewDialog.open({
              closeByBackdrop: false,
              type: 'completed'
            });
        }
      });
  }

  // 🦈fish-func runinng after delivery option clicked
  public getModalConfig() {
    this._stateService.continue$
      .pipe(
        untilDestroyed(this),
        filter(
          (isContinue: boolean) =>
            !this._stateService.isNullOrUndefined(isContinue)
        ),
        tap((isContinue: boolean) =>
          isContinue
            ? this.previewDialog.open({ closeByBackdrop: false, type: 'phone' })
            : null
        ),
        filter((isContinue: boolean) => !isContinue),
        switchMap((isContinue: boolean) =>
          this._httpService.create(this.searchState)
        )
      )
      .subscribe((state: DebitCardState) => {
        this.localState.applicationID = state.applicationID;
        this.searchState = null;
      });

    this._stateService.continueOtp$
      .pipe(
        untilDestroyed(this),
        switchMap((isContinueOtp: boolean) => {
          return isContinueOtp
            ? this._httpService.continue(this.localState)
            : [];
        }),
        filter((state: DebitCardState) => !!state),
        tap((state: DebitCardState) => {
          this.localState = state;
          this.patchForm(this.localState);
        })
      )
      .subscribe();
  }

  // handle address equality checkbox
  public handleAddressEquality(isEqual: boolean) {
    if (!this.rosaForm.controls['regAddress'].valid) {
      return;
    }
    if (isEqual) {
      this.localState.forms.application.fields.factAddress.value = this.localState.forms.application.fields.regAddress.value;
      this.factAddressValue.next(
        this.localState.forms.application.fields.factAddress.value
      );
      this.rosaForm.controls['factAddress'].updateValueAndValidity();
      return this.rosaForm.patchValue({
        factAddress: this.localState.forms.application.fields.factAddress.value
          .stringValue
      });
    } else {
      //this.localState.forms.application.fields.factAddress.value = null;
      //return this.rosaForm.patchValue({factAddress: null})
    }
  }
  private _setBirthDate(value: string) {
    this.userYear = Number(value.slice(4, 8));
    this.validatePassport();
  }
  private _setDocIssueDate(value: string) {
    this.passYear = Number(value.slice(4, 8));
    this.validatePassport();
  }
  private _setDate(date: string, type: 'birthDate' | 'docIssueDate') {
    if (this._stateService.isNullOrUndefined(date)) return;
    const convertedDate = this._stateService.formatDate(date, 'fromSiebel');
    return this.rosaForm.controls[type].setValue(convertedDate, {
      onlySelf: true,
      emitEvent: false
    });
  }
  // carry and run passport validation
  validatePassport() {
    if (!this.passYear || !this.userYear) return;
    return this.setPassFieldExpired(
      this._stateService.isPassportExpired(this.passYear, this.userYear)
    );
  }
  // toggle passport expired error
  private setPassFieldExpired(isNotExpired: boolean) {
    if (isNotExpired) {
      this.rosaForm.controls['docIssueDate'].updateValueAndValidity({
        onlySelf: true,
        emitEvent: true
      });
      return setTimeout(() => {
        this.errorMessages.docIssueDate = 'укажите дату выдачи паспорта';
      }, 1000);
    }
    this.rosaForm.controls['docIssueDate'].updateValueAndValidity({
      onlySelf: true,
      emitEvent: true
    });
    this.rosaForm.controls['docIssueDate'].setErrors({ incorrect: true });
    return (this.errorMessages.docIssueDate = 'паспорт просрочен');
  }

  // 📝parse FIO from form state to form value
  public parseName(value) {
    if (this._stateService.isNullOrUndefined(value)) return;
    const formValues = this._stateService.cleanObj(value, ['stringValue']);
    this.rosaForm.patchValue(formValues, { onlySelf: true, emitEvent: false });
  }

  // 📬parse address from form state to form value
  public parseAddress(value, type: 'reg' | 'fact') {
    if (this._stateService.isNullOrUndefined(value)) return;
    if (type === 'reg') {
      this.localState.forms.application.fields.regAddress.value = value;
      this.rosaForm.controls['regAddress'].setValue(value.stringValue, {
        onlySelf: true,
        emitEvent: false
      });
      this.regAddressValue.next(value);
      //this.rosaForm.controls['regAddress'].updateValueAndValidity()
    }
    if (
      type === 'fact' ||
      this.localState.forms.application.fields.factAddressEquality.value
    ) {
      this.localState.forms.application.fields.factAddress.value = value;
      this.rosaForm.controls['factAddress'].setValue(value.stringValue);
      this.factAddressValue.next(value);
      //this.rosaForm.controls['factAddress'].markAsTouched();
      //this.rosaForm.controls['factAddress'].updateValueAndValidity()
    }
  }

  public setPhone(phone, isEmitEvent = false) {
    if (this._stateService.isNullOrUndefined(phone)) {
      return;
    }
    const phoneConverted = this._stateService.convertPhone(phone);
    this.rosaForm.controls['mobilePhone'].setValue(phoneConverted, {
      onlySelf: true,
      emitEvent: isEmitEvent
    });
  }
  // 🏠parse address from dadata and update object
  public setRegAddress(address, type: 'reg' | 'fact') {
    const addressObj = this._stateService.makeAddressFromDadata(address);
    if (type === 'reg') {
      this.regAddressValue.next(addressObj);
      if (this.rosaForm.controls['factAddressEquality'].value) {
        this.rosaForm.controls['factAddress'].setValue(addressObj.stringValue);
        this.localState.forms.application.fields.factAddress.value = addressObj;
      }
      return (this.localState.forms.application.fields.regAddress.value = addressObj);
    }
    this.factAddressValue.next(addressObj);
    return (this.localState.forms.application.fields.factAddress.value = addressObj);
  }

  // form submit
  public handleAndSendForm(action: 'Courier' | 'Branch') {
    //return this.previewDialog.open({closeByBackdrop: false,  type: "disclaimers"})
    for (const c in this.rosaForm.controls) {
      if (this.rosaForm.controls[c]) {
        this.rosaForm.controls[c].markAsTouched();
        this.rosaForm.controls[c].updateValueAndValidity();
      }
    }
    if (!this.rosaForm.valid) return;
    this._stateService.applicationOtp$.next(false);
    this.localState.checkoutChannel = action;
    switch (action) {
      case 'Courier':
        this._stateService.deliveryAgreement$ = new BehaviorSubject(false);

        //return this.router.navigate(['/delivery'])

        this._stateService.deliveryAgreement$
          .pipe(
            untilDestroyed(this),
            tap((isAgree: boolean) => {
              !isAgree &&
                this.previewDialog.open({
                  closeByBackdrop: false,
                  type: 'agreements'
                });
            }),
            filter((isAgree: boolean) => {
              return !this._stateService.isNullOrUndefined(isAgree) && isAgree;
            }),
            tap(() => {
              this._setCourierData();
            }),
            switchMap(() => this._stateService.applicationOtp$),
            filter(
              (isOtp: boolean) =>
                !this._stateService.isNullOrUndefined(isOtp) && isOtp
            ),
            tap(() =>
              this.previewDialog.open({
                closeByBackdrop: false,
                type: 'disclaimers'
              })
            ),
            switchMap(() => this._stateService.presendAgreement$),
            filter((presend: boolean) => presend),
            tap(() => {
              this.sendButton.setState = `loading`;
            }),
            switchMap(() =>
              this._httpService.save(
                this._stateService.patchFormState(
                  this.localState,
                  this.rosaForm.value
                )
              )
            )
          )
          .subscribe((state: DebitCardState) => {
            this.sendButton.setState = `complete`;
            this._stateService.setState$(state);
            this.router.navigate(['/delivery'], {
              queryParamsHandling: 'merge'
            });
          });
        return;
      case 'Branch':
        // here sms sended second time
        // return this.router.navigate(['/office'])
        this._stateService.deliveryAgreement$.complete();
        this._stateService.otpStatus$.next('CHECKOUT');
        this.previewDialog.open({ closeByBackdrop: false, type: 'phone' });
        this._stateService.applicationOtp$
          .pipe(
            filter((isOtp: boolean) => {
              return !this._stateService.isNullOrUndefined(isOtp) && isOtp;
            }),
            tap(() =>
              this.previewDialog.open({
                closeByBackdrop: false,
                type: 'disclaimers'
              })
            ),
            switchMap(() => this._stateService.presendAgreement$),
            filter((presend: boolean) => presend),
            tap(() => {
              this.sendButton.setState = `loading`;
              this.isLoading = true;
            }),
            switchMap(value =>
              this._httpService.save(
                this._stateService.patchFormState(
                  this.localState,
                  this.rosaForm.value
                )
              )
            ),
            untilDestroyed(this)
          )
          .subscribe((state: DebitCardState) => {
            this.isLoading = false;
            this.sendButton.setState = `complete`;
            this._stateService.setState$(state);
            this.router.navigate(['/office'], { queryParamsHandling: 'merge' });
          });
        return;
    }
  }

  private _setCourierData() {
    this._stateService.otpStatus$.next('CHECKOUT');
    return this.previewDialog.open({ closeByBackdrop: false, type: 'phone' });
  }

  // 📐boilerplate for dadata search
  public dadataListener(
    fieldName: string,
    dadataSearchType: 'address' | 'party' | 'fio' | 'fms_unit' | 'fns_unit'
  ) {
    return this.rosaForm.get(fieldName).valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(100),
      switchMap(value => this._dadataService.search(value, dadataSearchType))
    );
  }

  // 📐boilerplate for dadataCloud search
  public dadataCloudListener(
    fieldName: string,
    dadataSearchType: 'fms_unit'
  ): Observable<any> {
    return this.rosaForm.get(fieldName).valueChanges.pipe(
      distinctUntilChanged(),
      debounceTime(100),
      switchMap(value =>
        this._dadataService.searchCloud(value, dadataSearchType)
      )
    );
  }

  // 📟initialize form
  public initForm(): void {
    this.rosaForm = this._fb.group({
      mobilePhone: [
        { value: ``, disabled: this.isDisabled },
        { validators: Validators.required, updateOn: 'blur' }
      ],
      email: [
        { value: ``, disabled: this.isDisabled },
        { validators: [Validators.required], updateOn: 'blur' }
      ],
      firstName: [
        { value: ``, disabled: this.isDisabled },
        {
          validators: [
            Validators.required,
            Validators.pattern('^[а-яА-ЯёЁ s-]+$'),
            ValidateSpaces
          ],
          updateOn: 'blur'
        }
      ],
      lastName: [
        { value: ``, disabled: this.isDisabled },
        {
          validators: [
            Validators.required,
            Validators.pattern('^[а-яА-ЯёЁ s-]+$'),
            ValidateSpaces
          ],
          updateOn: 'blur'
        }
      ],
      middleName: [
        { value: ``, disabled: this.isDisabled },
        {
          validators: [Validators.pattern('^[а-яА-ЯёЁ s-]+$'), ValidateSpaces],
          updateOn: 'blur'
        }
      ],
      gender: [
        { value: `MALE`, disabled: this.isDisabled },
        { validators: Validators.required, updateOn: 'change' }
      ],
      birthDate: [
        { value: ``, disabled: this.isDisabled },
        { validators: [Validators.required, ValidateAge], updateOn: 'blur' }
      ],
      birthPlace: [
        { value: ``, disabled: this.isDisabled },
        {
          validators: [
            Validators.required,
            Validators.pattern('^[а-яА-ЯёЁ0-9.,№ s-]+$'),
            ValidateSpaces
          ],
          updateOn: 'blur'
        }
      ],
      docSerNum: [
        { value: ``, disabled: this.isDisabled },
        { validators: Validators.required, updateOn: 'blur' }
      ],
      docIssueDate: [
        { value: ``, disabled: this.isDisabled },
        { validators: [Validators.required, ValidateAge], updateOn: 'blur' }
      ],
      docEntityCode: [
        { value: ``, disabled: this.isDisabled },
        { validators: [Validators.required], updateOn: 'blur' }
      ],
      docIssueEntity: [
        { value: ``, disabled: this.isDisabled },
        {
          validators: [
            Validators.required,
            Validators.pattern('^[а-яА-ЯёЁ0-9.,№() s-]+$'),
            ValidateSpaces
          ],
          updateOn: 'change'
        }
      ],
      method: [
        { value: `Branch`, disabled: this.isDisabled },
        { updateOn: 'change' }
      ],
      regAddress: [
        { value: ``, disabled: this.isDisabled },
        {
          validators: [
            Validators.required,
            ValidateAddress(this.regAddressValue),
            ValidateSpaces
          ],
          updateOn: 'change'
        }
      ],
      factAddressEquality: [
        { value: true, disabled: this.isDisabled },
        { updateOn: 'change' }
      ],
      factAddress: [
        { value: ``, disabled: this.isDisabled },
        {
          validators: [
            Validators.required,
            ValidateAddress(this.factAddressValue),
            ValidateSpaces
          ],
          updateOn: 'change'
        }
      ]
    });
  }
  ngOnDestroy() {}
}
