import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './homepage/homepage.component';
import { HomepageRoutingModule } from './homepage-routing.module';
import { FeaturesModule } from '../shared/features/features.module';
import { FormSharedModule } from '../shared/forms/forms-shared.module';
import { NgxMaskModule } from 'ngx-mask';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    HomepageComponent,

  ],
  imports: [
    CommonModule,
    FeaturesModule,
    HomepageRoutingModule,
    FormSharedModule,
    SharedModule,
    NgxMaskModule.forRoot(),
  ],
  exports: [
    HomepageComponent
  ],

})
export class HomepageModule { }
