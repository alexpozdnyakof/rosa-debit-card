import { HttpService, StateService, DadataService, ModalService } from './../../../services/';
import { DebitCardState } from '@monorepo/api-interfaces';
import { Component, OnInit, OnDestroy, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { distinctUntilChanged, tap, debounceTime, switchMap } from 'rxjs/operators';
import { Observable, BehaviorSubject } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { ValidateAddress, ValidateAge, ValidateSpaces } from '../../../shared/validators';
import { RosaButtonDirective } from 'libs/ui/src/lib/buttons/rosa-button.directive';
import { Router } from '@angular/router';

@Component({
  selector: 'rosa-delivery',
  templateUrl: './delivery.component.html',
  styleUrls: ['./delivery.component.scss']
})
export class DeliveryComponent implements OnInit, OnDestroy {
  public deliveryForm: FormGroup;
  private isDisabled = false;
  private _windowWidth = window.innerWidth;
  public localState: DebitCardState;
  public deliveryPlace$: Observable<any>;
  public deliveryAddressValue: BehaviorSubject<any> = new BehaviorSubject(null);
  public isLoading = false;
  @ViewChild('sendButton', {static: true, read: RosaButtonDirective})
  sendButton: RosaButtonDirective

  constructor(
    private _stateService: StateService,
    private _fb: FormBuilder,
    private _dadataService: DadataService,
    private _httpService: HttpService,
    private previewDialog: ModalService,
    private _router: Router
  ) { }

  ngOnInit() {
    this._stateService.getState$().pipe(tap((state:DebitCardState)=>{this.localState = state;})).subscribe()
    this.initForm();
    this.deliveryPlace$ = this.dadataListener('deliveryAddress', 'address')
  }
  ngOnDestroy(){}
  public dadataListener(fieldName: string, dadataSearchType: 'address' | 'party' | 'fio' | 'fms_unit' | 'fns_unit'){
    return this.deliveryForm
    .get(fieldName)
    .valueChanges
    .pipe(
        untilDestroyed(this),
        distinctUntilChanged(),
        debounceTime(100),
        switchMap(value => this._dadataService.search(value, dadataSearchType)
      )
    )
  }
  public routeBack(){
    this._router.navigate(['/'],  { queryParamsHandling: "merge" })
  }
  public initForm(): void {
    this.deliveryForm = this._fb.group({
      codeWord: ['', { validators: [Validators.required, Validators.pattern('^[а-яА-ЯёЁ0-9 \s-]+$'), ValidateSpaces], updateOn: 'blur', disabled: this.isDisabled }],
      deliveryAddress: ['', { validators: [Validators.required, ValidateAddress(this.deliveryAddressValue), ValidateSpaces], updateOn: 'change', disabled: this.isDisabled}],
      smsInfo: [true, { updateOn: 'change', disabled: this.isDisabled }],
      safeAcc: [true, { updateOn: 'change', disabled: this.isDisabled }],
      benefAbs: [true, { validators: Validators.requiredTrue, updateOn: 'change', disabled: this.isDisabled }],
      thirdPartyAbs: [true, { validators: Validators.requiredTrue, updateOn: 'change', disabled: this.isDisabled }],
    });
  }
  public setDeliveryAddress(address){
    const addressObj = this._stateService.makeAddressFromDadata(address);
      this.deliveryAddressValue.next(addressObj)
      return this.localState.forms.checkoutDelivery.fields.deliveryAddress.value = addressObj
  }
  public checkoutForm(){
    for(const c in this.deliveryForm.controls){
      if(this.deliveryForm.controls[c]){
        this.deliveryForm.controls[c].markAsTouched();
        this.deliveryForm.controls[c].updateValueAndValidity();
      }
    }
    if(!this.deliveryForm.valid) return;  // handle form invalid
    // set loading state
    this.sendButton.setState = `loading`
    this.isLoading = true;
    this._httpService.checkout(this._stateService.patchDeliveryFormState(this.localState, this.deliveryForm.value))
    .pipe(untilDestroyed(this))
    .subscribe(
      data =>{
        this.isLoading = false;
        this.sendButton.setState = `complete`;
        return this.previewDialog.open({closeByBackdrop: false,  type: "success"})
      })
  }
}
