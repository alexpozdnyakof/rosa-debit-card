import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DeliveryRoutingModule } from './delivery-routing.module';
import { DeliveryComponent } from './components/delivery/delivery.component';
import { UiModule } from '@rosa/ui';
import { FormSharedModule } from '../shared/forms/forms-shared.module';

@NgModule({
  declarations: [DeliveryComponent],
  imports: [
    CommonModule,
    DeliveryRoutingModule,
    FormSharedModule,
    UiModule
  ]
})
export class DeliveryModule { }
