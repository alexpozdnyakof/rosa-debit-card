import { SuccessComponent } from './components/modals/success/success.component';
import { CookieInterceptor } from './services/interceptors/cookie.interceptor';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UiModule } from '@rosa/ui';
import { AppComponent } from './app.component';
import { PageOneComponent } from './components/page-one/page-one.component';
import { PageTwoComponent } from './components/page-two/page-two.component';
import { FormComponent } from './components/form/form.component';
import { FormSharedModule } from './shared/forms/forms-shared.module';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { SharedModule } from './shared/shared.module';
import { HeaderComponent } from './components/header/header.component';
import { AgreementComponent } from './components/modals/agreement/agreement.component';
import { PhoneCodeComponent } from './components/modals/phone-code/phone-code.component';
import { ContinueComponent } from './components/modals/continue/continue.component';
import { FooterComponent } from './components/footer/footer.component';
import { CompleteComponent } from './components/modals/complete/complete.component';
import { DisclaimerComponent } from './components/modals/disclaimer/disclaimer.component';


@NgModule({
  declarations: [
    AppComponent,
    PageOneComponent,
    PageTwoComponent,
    FormComponent,
    HeaderComponent,
    AgreementComponent,
    PhoneCodeComponent,
    ContinueComponent,
    FooterComponent,
    SuccessComponent,
    CompleteComponent,
    DisclaimerComponent

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    UiModule,
    SharedModule,
    FormSharedModule,
  ],
  entryComponents: [
    AgreementComponent,
    PhoneCodeComponent,
    ContinueComponent,
    SuccessComponent,
    CompleteComponent,
    DisclaimerComponent
  ],
  providers: [
    {
      provide: 'API_URL',
      useValue: 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/party'
    },
    {
      provide: 'API_KEY',
      useValue: '5c0fa69268870b7061361c5bdfc30bc1b1e95846'
    },
    { provide: HTTP_INTERCEPTORS, useClass: CookieInterceptor, multi: true },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
