import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PageTwoComponent } from './components/page-two/page-two.component';
import { FormComponent } from './components/form/form.component';

const routes: Routes = [

  { path: 'page-2', component: PageTwoComponent },
  { path: 'form', component: FormComponent },
  {
    path: 'homepage',
    loadChildren: () => import('./homepage/homepage.module').then(m => m.HomepageModule),
    // canLoad: [AuthGuard]
  },
  {
    path: 'delivery',
    loadChildren: () => import('./delivery/delivery.module').then(m => m.DeliveryModule),
    // canLoad: [AuthGuard]
  },
  {
    path: 'office',
    loadChildren: () => import('./office/office.module').then(m => m.OfficeModule),
    // canLoad: [AuthGuard]
  },
  { path: '',
    loadChildren: () => import('./homepage/homepage.module').then(m => m.HomepageModule),
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { enableTracing: false })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
