import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UiModule } from '@rosa/ui';
import { FormSharedModule } from '../shared/forms/forms-shared.module';

import { OfficeRoutingModule } from './office-routing.module';
import { OfficeComponent } from './components/office/office.component';
import { SharedModule } from '../shared/shared.module';
import { NgxMaskModule } from 'ngx-mask';

@NgModule({
  declarations: [OfficeComponent],
  imports: [
  CommonModule,
    OfficeRoutingModule,
    FormSharedModule,
    SharedModule,
    UiModule,
    NgxMaskModule,
  ]
})
export class OfficeModule { }
