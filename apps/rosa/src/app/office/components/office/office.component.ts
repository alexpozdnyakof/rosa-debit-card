import { untilDestroyed } from 'ngx-take-until-destroy';
import {
  Component,
  OnInit,
  Renderer2,
  ViewChild,
  OnDestroy,
  AfterViewInit,
  ElementRef
} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RosaButtonDirective } from 'libs/ui/src/lib/buttons/rosa-button.directive';
import { tap } from 'rxjs/operators';
import { DebitCardState, Office, Schedule } from '@monorepo/api-interfaces';
import {
  HttpService,
  StateService,
  ModalService,
  MapService,
  ScrollService
} from '../../../services/';
import { ValidateAge } from '../../../shared/validators/ages.validator';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'rosa-office',
  templateUrl: './office.component.html',
  styleUrls: ['./office.component.scss']
})
export class OfficeComponent implements OnInit, OnDestroy, AfterViewInit {
  public officeForm: FormGroup;
  private isDisabled = false;
  public localState: DebitCardState;
  public isLoading = false;
  public office: Office;
  public office$: BehaviorSubject<Office> = this._stateService.office$;
  private _windowWidth = window.innerWidth;
  constructor(
    private _mapService: MapService,
    private _renderer: Renderer2,
    private _fb: FormBuilder,
    private previewDialog: ModalService,
    private _stateService: StateService,
    private _scrollService: ScrollService,
    private _httpService: HttpService,
    private _router: Router
  ) {}
  @ViewChild('baloon', { static: true }) baloon: ElementRef;
  @ViewChild('sendButton', { static: true, read: RosaButtonDirective })
  sendButton: RosaButtonDirective;
  /*@ViewChild('baloonButton', { static: false, read: RosaButtonDirective })
  baloonButton: RosaButtonDirective;*/
  @ViewChild('datepickerZone', { static: true }) datepickerZone: ElementRef;
  @ViewChild('mapZone', { static: true }) mapZone: ElementRef;
  public isWeekendAvailable = {
    saturday: false,
    sunday: false
  };

  ngAfterViewInit(): void {}
  public toggleBaloon(office: Office) {
    if (office !== null) {
      this.officeForm.controls['visitBranch'].setValue(`${office.code}`);
      this.isAvailableOnWeekend(office.schedule);
      /*this._mapService.calculateRoute(
        [55.7729081421, 37.647920084655],
        [55.768307, 37.6478]
      );*/
    }

    return office === null
      ? this._renderer.removeClass(this.baloon.nativeElement, 'baloon_open')
      : this._renderer.addClass(this.baloon.nativeElement, 'baloon_open');
  }

  public isAvailableOnWeekend(schedule: Schedule[]) {
    const days = schedule
      .map((scheduleObj: Schedule) => scheduleObj.days)
      .toString()
      .toLowerCase();
    return (this.isWeekendAvailable = {
      saturday: days.indexOf('сб') > -1,
      sunday: days.indexOf('вс') > -1
    });
  }

  public scrollToMapOnMobile(office) {
    if (office === null) {
      return;
    }
    //console.log(office)
    const mapPosition = this.mapZone.nativeElement.getBoundingClientRect();
    const position = this.mapZone.nativeElement.getBoundingClientRect();
    if (this._windowWidth < 420 && position.top > 10) {
      this._scrollService.scrollTo(this.mapZone.nativeElement);
    }
  }
  ngOnInit() {
    this._stateService
      .getState$()
      .pipe(
        untilDestroyed(this),
        tap((state: DebitCardState) => {
          this.localState = state;
        })
      )
      .subscribe();
    this._mapService
      .getMap('map', [55.751574, 37.573856])
      .pipe(untilDestroyed(this))
      .subscribe();
    this._stateService.office$
      .pipe(untilDestroyed(this))
      .subscribe((office: Office) => {
        this.office = office;
        this.toggleBaloon(office);
        this.scrollToMapOnMobile(office);
      });
    this.initForm();
  }
  ngOnDestroy() {}

  public handleForm() {
    for (const c in this.officeForm.controls) {
      if (this.officeForm.controls[c]) {
        this.officeForm.controls[c].markAsTouched();
        this.officeForm.controls[c].updateValueAndValidity();
      }
    }
    const selectedDate = new Date(
      this._stateService.formatDate(this.officeForm.controls['visitDate'].value)
    );
    if (
      (selectedDate.getDay() === 5 && !this.isWeekendAvailable.saturday) ||
      (selectedDate.getDay() === 6 && !this.isWeekendAvailable.sunday)
    ) {
      return alert(
        'Выбранный офис не работает в выходные дни. Вы можете выбрать другой офис или изменить день визита'
      );
    }
    if (!this.officeForm.valid) return;
    this.sendButton.setState = `loading`;
    this.isLoading = true;
    this._httpService
      .checkout(
        this._stateService.patchBranchFormState(
          this.localState,
          this.officeForm.value
        )
      )
      .subscribe(data => {
        this.isLoading = false;
        this.sendButton.setState = `complete`;
        return this.previewDialog.open({
          closeByBackdrop: false,
          type: 'success'
        });
      });
  }
  public initForm(): void {
    this.officeForm = this._fb.group({
      visitDate: [
        { value: '', disabled: this.isDisabled },
        { validators: [Validators.required], updateOn: 'change' }
      ],
      visitBranch: [
        { value: '', disabled: this.isDisabled },
        { validators: Validators.required, updateOn: 'change' }
      ]
    });
  }
  public closeBaloon() {
    this.officeForm.controls['visitBranch'].setValue(``);
    this._mapService.toggleOffice(false);
  }
  public setOffice() {
    this._scrollService.scrollTo(this.datepickerZone.nativeElement);
  }
  public changeZoom(zoomDirection: 'in' | 'out') {
    this._mapService.toggleOffice(false);
    this._mapService.changeZoom(zoomDirection);
  }
  public routeBack() {
    this._router.navigate(['/'], { queryParamsHandling: 'merge' });
  }
}
