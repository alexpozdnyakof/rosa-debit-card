import { Component, ViewChild, ElementRef, HostListener } from '@angular/core';
import { Router, NavigationEnd, NavigationStart } from '@angular/router';
import { HttpService } from './services/http.service';
import { CookieService } from './services';
//export declare const ga: any;
declare const ga: Function;

@Component({
  selector: 'rosa-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'analytics-analytics';
  @ViewChild('contentArea', {static: true}) private contentArea: ElementRef<HTMLDivElement>;
  @HostListener('window:beforeunload', ['$event'])
  canLeavePage($event: any) {
    this._cookieService.delete('esiaState');
  }

  constructor(
    public router: Router,
    private _httpService: HttpService,
    private _cookieService: CookieService
    ){

    this.router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        //document.cookie = 'esiaState'+'=; Max-Age=-99999999;';
      }
      if (event instanceof NavigationEnd) {
        window.scrollTo(0,0);
        //document.cookie = 'esiaState'+'=; Max-Age=-99999999;';
        //console.log(this.contentArea.nativeElement.scrollTop);
        this.contentArea.nativeElement.scrollTop = 0;
        ga('set', 'page', event.urlAfterRedirects);
        ga('send', 'pageview');
      }
    })
  }
}
