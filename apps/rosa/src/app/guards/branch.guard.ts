import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { StateService } from './../services/state/state.service';
import { filter } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class BranchGuard implements CanActivate {

  constructor(private _stateService: StateService, private router: Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
    let canBranch = false;
    this._stateService.applicationOtp$.pipe(
      filter((isOtp: boolean) => isOtp !== null && isOtp),
    ).subscribe(
      data => {canBranch = true}
    )
    if(!canBranch){
      window.alert("You don't have permission to view this page")
      this.router.navigate(['/']);
    }
    return canBranch ? true : false;
  }

}
