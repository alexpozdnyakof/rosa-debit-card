import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable, combineLatest } from 'rxjs';
import { StateService } from '../services/state/state.service';
import { filter } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DeliveryGuard implements CanActivate {

  constructor(private _stateService: StateService, private router: Router){}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    return true;
    let canDelivery = false;
    combineLatest([
      this._stateService.applicationOtp$,
      this._stateService.deliveryAgreement$
    ]).pipe(
      filter((config: Array<boolean>) => config[0] !== null && config[1] !== null),
      filter((config: Array<boolean>) => config[0]&&config[1])
    ).subscribe(
      data => {canDelivery = true}
    )
    if(!canDelivery){
      //window.alert("You don't have permission to view this page")
      this.router.navigate(['/']);
    }
    return canDelivery ? true : false;
  }

}

