import { TestBed, async, inject } from '@angular/core/testing';

import { DeliveryGuard } from './delivery.guard';

xdescribe('DeliveryGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DeliveryGuard]
    });
  });

  it('should ...', inject([DeliveryGuard], (guard: DeliveryGuard) => {
    expect(guard).toBeTruthy();
  }));
});
