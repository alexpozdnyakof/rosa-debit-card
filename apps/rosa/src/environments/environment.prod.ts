export const environment = {
  production: true,
  api: {
    offices: 'https://store.rosbank.ru/rest/offices',
    endpoint:  'https://store-test.rosbank.ru:62443/dst-api-proxy/debit-card/v1',
    parsed: 'http://localhost:3333/rest/debit-card-api/start',
    card: 'https://api.rosbank.ru/private-person/card/debit'
  },
  esia: 'https://store-test.rosbank.ru:62443/esia-get2'
};

