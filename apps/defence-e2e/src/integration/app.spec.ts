import { getGreeting } from '../support/app.po';

describe('defence', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to defence!');
  });
});
