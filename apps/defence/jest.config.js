module.exports = {
  name: 'defence',
  preset: '../../jest.config.js',
  coverageDirectory: '../../coverage/apps/defence',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
