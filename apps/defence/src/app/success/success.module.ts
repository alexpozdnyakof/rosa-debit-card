import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SuccessComponent } from './success/success.component';
import { SuccessRoutingModule } from './success-routing.module';
import { UiModule } from '@monorepo/ui';

@NgModule({
  declarations: [SuccessComponent],
  imports: [
    CommonModule,
    SuccessRoutingModule,
    UiModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SuccessModule {}
