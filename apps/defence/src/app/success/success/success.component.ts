import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'monorepo-success',
  templateUrl: './success.component.html',
  styleUrls: ['./success.component.scss']
})
export class SuccessComponent implements OnInit {
  public mailForm: FormGroup;
  public isDisabled = false;
  constructor(private _fb: FormBuilder) {}

  ngOnInit() {
    this.initForm();
  }
  public initForm(): void {
    this.mailForm = this._fb.group({
      email: [
        { value: ``, disabled: this.isDisabled },
        { validators: [Validators.required], updateOn: 'blur' }
      ]
    });
  }
  public sendMail() {
    return;
  }
}
