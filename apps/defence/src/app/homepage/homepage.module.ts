import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './homepage/homepage.component';
import { HomepageRoutingModule } from './homepage-routing.module';
import { UiModule } from '@monorepo/ui';



@NgModule({
  declarations: [HomepageComponent],
  imports: [
    CommonModule,
    UiModule,
    HomepageRoutingModule
  ]
})
export class HomepageModule { }
