import { Router } from '@angular/router';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { of } from 'rxjs';
import { ScrollService } from '../../services';

@Component({
  selector: 'monorepo-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  @ViewChild('tabsElement', { static: true })
  tabsElement: ElementRef;
  public activeTab: 'defence-list' | 'defence-compare' = 'defence-list';
  constructor(private router: Router, private _scrollService: ScrollService) {}
  public setTab(tab) {
    this.activeTab = tab;
  }
  ngOnInit() {}

  public selectPackage(type: string) {
    this.router.navigate(['/account']);
    return type;
  }
  public switchTabs(tabValue: 'defence-list' | 'defence-compare') {
    // scroll to tabs
    this._scrollService.scrollTo(this.tabsElement.nativeElement, {
      block: 'start',
      behavior: 'smooth'
    });
    return setTimeout(() => {
      this.setTab(tabValue);
    }, 300);
  }
}
