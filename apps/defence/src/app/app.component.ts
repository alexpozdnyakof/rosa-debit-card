import { Router, NavigationEnd } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'monorepo-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public step: 'info' | 'account' | 'summary' = 'info';
  title = 'defence';
  constructor(private router: Router) {}

  ngOnInit() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        window.scrollTo(0, 0);
        this._routeHandler(this.router.url.replace('/', ''));
      }
    });
  }

  private _routeHandler(route) {
    if (route.length === 0) {
      return (this.step = 'info');
    }
    if (route === 'success') {
      return (this.step = 'summary');
    }
    return (this.step = route);
  }
}
