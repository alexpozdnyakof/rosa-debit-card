import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TrimmerPipe } from './pipes/trimmer.pipe';
import { PhoneFormatterPipe } from './pipes/phone-formatter.pipe';
import { ClockComponent } from './icons/clock/clock.component';
import { CardComponent } from './icons/card/card.component';
import { HandsComponent } from './icons/hands/hands.component';
import { DocsComponent } from './icons/docs/docs.component';



@NgModule({
  declarations: [
    TrimmerPipe,
    PhoneFormatterPipe,
    ClockComponent,
    CardComponent,
    HandsComponent,
    DocsComponent,
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TrimmerPipe,
    PhoneFormatterPipe,
  ]
})
export class SharedModule { }
