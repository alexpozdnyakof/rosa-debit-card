import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'trimmer'
})
export class TrimmerPipe implements PipeTransform {

  transform(value: string, ...args: any[]): any {
    if(!value) return;
    return value.replace(/'/g, "&apos;").replace(/"/g, "");;
  }

}
