import { AbstractControl, ValidatorFn } from '@angular/forms';

export function ValidateSpaces(control: AbstractControl) {
    if(control.value.length === 0){ return }
    const trimmedValue = control.value.replace(/\s/g,'');
    return trimmedValue.length > 0 ? null :  { invalidValue: true};
}


