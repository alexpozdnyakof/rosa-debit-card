import { AbstractControl, ValidatorFn } from '@angular/forms';
import { filter } from 'rxjs/operators';

export function ValidateName(value: any): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    let isValid = true;
    value
    .pipe(filter((fio) => fio !== null))
    .subscribe(
        data => {
            console.log(value);
            isValid = control.value.split(' ').length === 3;
        }
    )
    return isValid ? null :  { validName: true};
  }
}
