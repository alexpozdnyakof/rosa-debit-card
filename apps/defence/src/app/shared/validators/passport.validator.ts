import { ValidatorFn, FormGroup, AbstractControl, AsyncValidatorFn, ValidationErrors } from "@angular/forms";
import { distinctUntilChanged, filter, debounceTime, map } from 'rxjs/operators';
import { Observable, of, combineLatest } from 'rxjs';


export function ValidatePassportAsync(birthDateField): AsyncValidatorFn {
    return (control: AbstractControl): Observable <ValidationErrors | null> => {
      const controlValue = of(control.value);
      const validations: Observable<string>[] = [];
      validations.push(controlValue)
      validations.push(birthDateField)

      return combineLatest(validations).pipe(
        debounceTime(500),
        map(([passDate, birthDate])=> {
        console.log(passDate);
        console.log(birthDate)
        console.log(passportExpired(Number(birthDate.slice(4,8)), Number(passDate.slice(4,8))))
        return passportExpired(Number(birthDate.slice(4,8)), Number(passDate.slice(4,8))) ?  null : {'expired': true};
      })
      )
    };
  }
export function ValidatePassport(birthDate): ValidatorFn {
  return (control: AbstractControl):  {[key: string]: any} | null => {
    let birthDateData;
    birthDate.pipe(
        distinctUntilChanged(),
      filter((value: string) => value && value.length === 8),
    ).subscribe(
      data => {
        console.log(birthDateData)
        birthDateData = data;
        //return passportExpired(Number(data.slice(4,8)), Number(control.value.slice(4,8))) ? null : {'expired': true};
    })
    console.log(`cVal: ${control.value}`)
    console.log(`bVal: ${birthDateData}`)
    if(!control.value || !birthDateData) {return null}

    return !passportExpired(Number(birthDateData.slice(4,8)), Number(control.value.slice(4,8))) ? {'expired': true} : null;

  }
}
/*
function validateField(birthDateData, control){
  console.log(`bd ${birthDateData}`);
  if(!control.value) {return}
  console.log(control.errors)
  if(!passportExpired(Number(birthDateData.slice(4,8)), Number(control.value.slice(4,8)))){
    console.log(`return {'expired': true}`)
    return  {'expired': true};
  }
  console.log(`return null`)
  return null;
}
*/
// Number(value.slice(4,8)
function passportExpired(userBirthYear, passYear){
  console.log(userBirthYear, passYear);
    const today = new Date();
    const year = Number(today.getFullYear())
    const userAges = year - userBirthYear
    const yearPoints = {
      years14: userBirthYear + 14,
      years20: userBirthYear + 20,
      years45: userBirthYear + 45,
    }
    if(userAges >= 14 && userAges < 20){
      return passYear >= yearPoints.years14 && passYear< yearPoints.years20;
    }
    if(userAges >= 20 && userAges < 45){
      return passYear >= yearPoints.years20 && passYear < yearPoints.years45;
    }
    if(userAges >= 45){
      return passYear >= yearPoints.years45;
    }
}
