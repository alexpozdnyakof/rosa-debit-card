import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'monorepo-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  public accounts = [
    {
      num: '40815836295003718245',
      amount: '30 000,00 RUB'
    },
    {
      num: '40815836295003711474',
      amount: '30 000,00 RUB'
    },
    {
      num: '40815836295003718245',
      amount: '30 000,00 RUB'
    },
    {
      num: '40815836295003711474',
      amount: '30 000,00 RUB'
    },
    {
      num: '40815836295003718245',
      amount: '30 000,00 RUB'
    },
    {
      num: '40815836295003711474',
      amount: '30 000,00 RUB'
    }
  ];
  public amount = 500;
  public selectAccount() {
    this.router.navigate(['/summary']);
  }
  constructor(private router: Router) {}

  ngOnInit() {}
  public routeBack() {
    this.router.navigate(['/']);
  }
}
