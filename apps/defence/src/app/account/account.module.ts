import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccountComponent } from './account/account.component';
import { AccountRoutingModule } from './account-routing.module';
import { UiModule } from '@monorepo/ui';

@NgModule({
  declarations: [AccountComponent],
  imports: [CommonModule, AccountRoutingModule, UiModule]
})
export class AccountModule {}
