import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'monorepo-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {
  public docs = [
    {
      title: 'Страховой полис',
      size: 'pdf, 250kb'
    },
    {
      title: 'Правила страхования',
      size: 'pdf, 250kb'
    }
  ];
  constructor(private router: Router) {}

  ngOnInit() {}
  public approve() {
    this.router.navigate(['/success']);
  }
  public routeBack() {
    this.router.navigate(['/account']);
  }
}
