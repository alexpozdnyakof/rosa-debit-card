import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SummaryComponent } from './summary/summary.component';
import { SummaryRoutingModule } from './summary-routing.module';
import { UiModule } from '@monorepo/ui';

@NgModule({
  declarations: [SummaryComponent],
  imports: [CommonModule, SummaryRoutingModule, UiModule]
})
export class SummaryModule {}
