import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './shared/shared.module';
import { UiModule } from '@monorepo/ui';
import { FormSharedModule } from './shared/forms/forms-shared.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    UiModule,
    SharedModule,
    FormSharedModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {}
