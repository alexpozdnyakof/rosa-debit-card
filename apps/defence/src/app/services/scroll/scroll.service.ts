import { Injectable } from '@angular/core';
import * as smoothscroll from './polyfill';
@Injectable({
  providedIn: 'root'
})
export class ScrollService {
  constructor() {
    smoothscroll.polyfill();
  }
  public scrollTo(element, options = { block: 'center', behavior: 'smooth' }) {
    element.scrollIntoView(options);
  }
  /*
  public scrollTo(element?) {
    let start = null;
    if (element && element.getBoundingClientRect().top === 0) {
      return;
    }
    console.log(element.getBoundingClientRect().top);
    const target = element && element ? element.getBoundingClientRect().top : 0;
    console.log(target);
    const firstPos = window.pageYOffset || document.documentElement.scrollTop;
    console.log(firstPos);
    let pos = 0;
    (function() {
      const browser = ['ms', 'moz', 'webkit', 'o'];
      for (
        let x = 0, length = browser.length;
        x < length && !window.requestAnimationFrame;
        x++
      ) {
        window.requestAnimationFrame =
          window[browser[x] + 'RequestAnimationFrame'];
        window.cancelAnimationFrame =
          window[browser[x] + 'CancelAnimationFrame'] ||
          window[browser[x] + 'CancelRequestAnimationFrame'];
      }
    })();
    function showAnimation(timestamp) {
      if (!start) {
        start = timestamp || new Date().getTime();
        console.log(start);
      } //get id of animation
      const elapsed = timestamp - start;
      const progress = elapsed / 800; // animation duration 600ms
      const outQuad = function(n) {
        return n * (2 - n);
      };
      const easeInPercentage = +outQuad(progress).toFixed(2);

      // if target is 0 (back to top), the position is: current pos + (current pos * percentage of duration)
      // if target > 0 (not back to top), the positon is current pos + (target pos * percentage of duration)
      console.log(target);
      console.log(easeInPercentage);
      pos =
        target === 0 || target < 0
          ? firstPos - target * easeInPercentage
          : firstPos + target * easeInPercentage;
      console.log(firstPos);
      console.log(target);
      console.log(firstPos - target * easeInPercentage);
      console.log(pos);
      window.scrollTo(0, pos);
      if (
        (target !== 0 && pos >= firstPos + target) ||
        (target === 0 && pos <= 0)
      ) {
        cancelAnimationFrame(start);
        if (element) {
          //element.setAttribute("tabindex", -1);
          //element.focus();
        }
        pos = 0;
      } else {
        window.requestAnimationFrame(showAnimation);
      }
    }
    window.requestAnimationFrame(showAnimation);
  }*/
}
