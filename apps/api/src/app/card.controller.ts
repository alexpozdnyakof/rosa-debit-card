import { Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller(`card-info`)
export class CardController {
  constructor(private readonly appService: AppService) {}
  @Get('/')
  getCard(): any {
        return  {
            name: 'Visa Signature #МожноВСЕ',
            description: 'Cashback и Travel-бонусы в одной карте. Что получать — решать вам!',
            image: 'https://api.rosbank.ru/uploads/card_picture/3/images/5cdc09692f0b9/contain_1200x750.jpg',
            options: [
              {
                title: 'до 12%',
                description: 'Cashback на 1 выбранную категорию'
              },
              {
                title: '5',
                description: 'Travel-бонусов начисляем за каждые 100 ₽'
              },
              {
                title: 'до 8%',
                description: 'на остаток по #МожноСЧЁТУ'
              }
            ]
          }
  }
}




