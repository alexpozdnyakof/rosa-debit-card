import { Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { map } from '@rosa/data';

@Controller(`debit-card-api`)
export class ServiceController {
  constructor(private readonly appService: AppService) {}
  @Get('start')
  getForm(): any {
    return this.appService.getContinue();
  }
}



