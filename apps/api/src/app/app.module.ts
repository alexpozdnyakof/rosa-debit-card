import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MapController } from './map.controller';
import { ServiceController } from './service.controller';
import { CardController } from './card.controller';

@Module({
  imports: [],
  controllers: [
    AppController,
    MapController,
    ServiceController,
    CardController
  ],
  providers: [AppService]
})
export class AppModule {}
