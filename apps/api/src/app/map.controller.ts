import { Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';
import { map } from '@rosa/data';

@Controller(`offices`)
export class MapController {
  constructor(private readonly appService: AppService) {}
  @Post('/')
  getBranches(): any {
    return map;
  }
}
