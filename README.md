# RosaUI Monorepo

This project was generated using [Nx](https://nx.dev).

<p align="center"><img src="https://raw.githubusercontent.com/nrwl/nx/master/nx-logo.png" width="450"></p>

🔎 **Nx is a set of Extensible Dev Tools for Monorepos.**

## Сборка фронтенда дебитовой карты

- `npm install`
- `ng build --project=rosa --base-href "абсолютный или относительный url" --prod --aot`

