export interface OtpRequestConfig {
    phone: string;
    subject: string;
  }
  export interface OtpResponseConfig {
    otp: string;
    subject: string;
  }