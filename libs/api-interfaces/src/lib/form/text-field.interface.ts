export interface TextField {
    caption: string;
    defaultValue: string;
    disabled: boolean;
    errorText: string;
    hint: string;
    id: string;
    mask: string;
    maxLength: number;
    minLength: number;
    placeholder: string;
    readOnly: boolean;
    regExp: string;
    required: boolean;
    tooltip: string;
    type: string;
    value: string;
}