export interface AddressField{
    caption: string;
    disabled: boolean;
    errorText: string;
    hint: string;
    id: string;
    maxLength: number;
    placeholder: string;
    readOnly: boolean;
    required: boolean;
    tooltip: string;
    type: string;
    value: AddressValue;
}
export interface AddressValue {
    stringValue: string;
    country: string;
    postalCode: string;
    region: string;
    districtType: string;
    district: string;
    cityType: string;
    city: string;
    settlementType: string;
    settlement: string;
    streetType: string;
    street: string;
    house: string;
    building: string;
    apartment: string;
}