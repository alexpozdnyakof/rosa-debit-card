
export interface CheckboxField {
    active: boolean;
    caption: string;
    disabled: boolean;
    errorText: string;
    hint: string;
    id: string;
    placeholder: string;
    readOnly: boolean;
    required: boolean;
    tooltip: string;
    type: string;
    value: boolean;
}