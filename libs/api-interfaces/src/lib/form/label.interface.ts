export interface Label{
    caption: string;
    dictionaryID: string;
    disabled: boolean;
    errorText: string;
    hint: string;
    id: string;
    placeholder: string;
    readOnly: string;
    required: boolean;
    tooltip: string;
    type: string;
    value: string;
}