export interface Button{
    actionType: string;
    caption: string;
    disabled: boolean;
    errorText: string;
    hint: string;
    id: string;
    placeholder: string;
    readOnly: boolean;
    required: boolean;
    tooltip: String;
    type: string;
}