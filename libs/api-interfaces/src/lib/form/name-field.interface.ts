
export interface NameField{
    caption: string;
    disabled: boolean;
    errorText: string;
    hint: string;
    id: string;
    mask: string;
    placeholder: string;
    readOnly: boolean;
    regExp: string;
    required: boolean;
    tooltip: string;
    type: string;
    value: NameValue;
}

export interface NameValue {
    stringValue: string;
    lastName: string;
    firstName: string;
    middleName: string;
    gender: string;
}