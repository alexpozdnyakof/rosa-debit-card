

export interface DateField {
    caption: string;
    disabled: boolean
    errorText: string;
    hint: string;
    id: string;
    mask: string;
    maxLength: number;
    placeholder: string;
    readOnly: boolean;
    required: boolean;
    tooltip: string;
    type: string;
    value: string;
}