export interface Card {
    title: string;
    card: {
        image: string;
        description: string;
        details: Array<CardHint>;
    }
}
export interface CardHint {
    title: string;
    description: string;
    hint: string;
}