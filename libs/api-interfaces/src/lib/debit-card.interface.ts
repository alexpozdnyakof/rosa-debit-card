import {
    AddressField,
    CheckboxField,
    TextField,
    DateField,
    NameField,
    Button,
    Label
} from './form/index';
export interface DebitCardState{
    deliveryAvailable: boolean;
    appChannel: string;
    applicationID: string;
    checkoutChannel: string;
    currentAction: string;
    currentForm: string;
    ebsAuth: string;
    esiaAuth?: {
        authorized: boolean;
    };
    prefilled?: boolean;
    ibBanner: string;
    cardCategory: string;
    codePBU: string;
    info: string;
    mpCode: string;
    otpCode: string;
    pid: string;
    productCode: string;
    srID: string;
    status: string;
    utm: string;
    forms: {
        application?:{
            fields: {
                adsFlag: CheckboxField;
                birthDate: DateField;
                birthPlace: TextField;
                bkiFlag: CheckboxField;
                docEntityCode: TextField;
                docIssueDate: DateField;
                docIssueEntity: TextField;
                docSerNum: TextField;
                factAddress: AddressField;
                factAddressEquality: CheckboxField;
                fio: NameField;
                email: TextField;
                mobilePhone: TextField;
                nextBranch: Button;
                nextDelivery: Button;
                pdFlag: CheckboxField;
                productName: any;
                regAddress: AddressField;
                searchButton: Button;
                tlcFlag: CheckboxField;
                userLocation: TextField;
                userTimezone: TextField;
                id: string;
            }
        };
        checkoutBranch: {
            fields: {
                nextButton: Button;
                prevButton: Button;
                visitBranch: TextField;
                visitDate: DateField;
            }
            id: string;
        };
        checkoutDelivery: {
            fields: {
                codeWord: TextField;
                deliveryAddress: AddressField;
                nextButton: Button;
                pepRelations: Label;
                pepStatus: Label;
                prevButton: Button;
                resPermit: Label;
                residence: Label;
                benefAbs:CheckboxField;
                safeAcc: CheckboxField;
                smsInfo: CheckboxField;
                stAcceptance: CheckboxField;
                taxStatus: Label;
                thirdPartyAbs: CheckboxField;
            }
            id: string;
        };

    }
}
