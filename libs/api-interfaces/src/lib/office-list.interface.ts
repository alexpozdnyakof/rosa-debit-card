export interface Phone{
    description: string;
    number: string;
}

export interface Schedule{
    days: string;
    hours: string;
}

export interface Office{
    address: string;
    code?: string;
    coords: Array<Number>;
    metro: Array<String>;
    misc: Array<String>;
    name: string;
    schedule: Array<Schedule>;
}

export interface OfficeList{
    phones: Array<Phone>;
    elements: Array<Office>;
}