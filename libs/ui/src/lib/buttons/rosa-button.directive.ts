import { Directive, Input, HostBinding, OnInit, Renderer2, ElementRef, OnChanges } from '@angular/core';

export interface ButtonIconConfig {
    position: 'left' | 'right',
    icon: string
}
@Directive({
  selector: 'button[rosaButton], a[rosaButton]'
})
export class RosaButtonDirective implements OnInit {
  private _type = `primary`;
  private _state;
  private _icon: Partial<ButtonIconConfig>;
  public text: string;
  public isDisabled: Boolean = false;
  @Input('rosaButton')
  set setType(value: string) {
    this._type = value || 'primary';
  }

  public set setText(value) {
    this.text = value;
    this.rewriteHTML();
  }

  @Input('buttonState')
  set setState(state: string) {
    this._state = state || `default`;
    this.chooseState();
  }

  @Input('buttonIcon')
  set setIcon(icon: ButtonIconConfig) {
    this._icon = icon || {};
  }

  constructor(
    private el: ElementRef,
    private renderer :Renderer2
  ) {
   }

   ngOnInit() {
     switch(this._type){
        case `primary`:
            this.renderer.addClass(this.el.nativeElement, 'rosa-button_primary');
            break;
        case `secondary`:
            this.renderer.addClass(this.el.nativeElement, 'rosa-button_secondary');
            break;
        case `tertiary`:
            this.renderer.addClass(this.el.nativeElement, 'rosa-button_tertiary');
            break;
        case `link`:
            this.renderer.addClass(this.el.nativeElement, 'rosa-button_link');
            break;
     }
      this.text = this.el.nativeElement.innerHTML; // сохраним текущее содержимое кнопки чтобы не потерять при очистке
      this.rewriteHTML();
    //this.el.nativeElement.innerHtml = this.ratingHtml;
  }

  public chooseState(){
    switch(this._state){
      case `active`:
        this.renderer.addClass(this.el.nativeElement, 'rosa-button_active');
        break;
      case `loading`:
        this.el.nativeElement.disabled = true;
        this.renderer.addClass(this.el.nativeElement, 'rosa-button_loading');
        break;
      case `complete`:
          this.renderer.addClass(this.el.nativeElement, 'rosa-button_complete');
          break;
      case `default`:
         this.renderer.removeClass(this.el.nativeElement, 'rosa-button_active');
         this.renderer.addClass(this.el.nativeElement, 'rosa-button_default');
         break;
    }
  }
  public rewriteHTML(){
    if(this._type === `link`) return;
    this.renderer.setProperty(this.el.nativeElement, 'innerHTML', ''); // clean html for reusable
    const html = `
      <svg width="24" height="24" viewBox="0 0 24 24" fill="none" class="rosa-button__complete-icon">
        <path fill-rule="evenodd" clip-rule="evenodd" d="M21.744 5.67L10.23 18.44l-6.926-6.722 1.393-1.435 5.437 5.277L20.258 4.33l1.486 1.34z" fill="#FFF">
      </svg>
      <div class="rosa-button__spinner"></div>
      <span class="rosa-button__overlay"></span>
      <span class="rosa-button__text">${this.text}</span>
    `;
    this.renderer.setProperty(this.el.nativeElement, 'innerHTML', html);
  }

}

