import { Directive, OnInit, Renderer2, ElementRef } from '@angular/core';

@Directive({
  selector: 'button[rosaRosaButtonAlt], a[rosaRosaButtonAlt]'
})
export class RosaButtonAltDirective implements OnInit {
  private _type = `primary`;
  constructor(
    private el: ElementRef,
    private renderer :Renderer2
  ) { }

  public ngOnInit(): void {
     switch(this._type){
        case `primary`:
          this.renderer.addClass(this.el.nativeElement, 'rosa-button__alt');
        break;
     }
  }
}
