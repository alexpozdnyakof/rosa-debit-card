import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RosaButtonDirective } from './rosa-button.directive';
import { RosaButtonAltDirective } from './rosa-button-alt.directive';

@NgModule({
  declarations: [
    RosaButtonDirective, 
    RosaButtonAltDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    RosaButtonDirective,
    RosaButtonAltDirective
  ],
})
export class ButtonModule { }
