import { Component, OnInit, ChangeDetectionStrategy, Renderer2 } from '@angular/core';

@Component({
  selector: 'rosa-icon-help',
  template: `<svg width="16px" height="16px" viewBox="0 0 16 16"><circle id="Oval" fill="#c4c4c4" fill-rule="nonzero" cx="8" cy="8" r="8"></circle><path d="M8,11.332 L8,12.6654" id="Path" stroke="#FFFFFF" stroke-width="1.5"></path><path d="M6,6 L6,5.33333 C6,4.54856 6.46561,4 7.24444,4 L9.42222,4 C10.2011,4 10.6667,4.64829 10.6667,5.43306 L10.6667,6.33333 C10.6667,7.1181 10.2011,7.67194 9.42222,7.67194 C8.64339,7.67194 8,8.21523 8,9 L8,10" fill="none" stroke="#FFFFFF" stroke-width="1.5"></path></svg>`,
  styles: [':host{display: block; width: 16px; height: 16px;}'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HelpComponent implements OnInit {
  private _fill = "#C4C4C4";
  constructor(private _renderer: Renderer2) { }

  ngOnInit() {
  }
  set fill(color: string){
    this._fill = color;
  }
  get color(){
    return this._fill;
  }

}
