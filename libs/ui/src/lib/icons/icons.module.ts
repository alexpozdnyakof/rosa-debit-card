import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IconDropdownComponent } from './icon-dropdown/icon-dropdown.component';
import { IconCheckboxComponent } from './checkbox/checkbox.component';
import { CloseComponent } from './close/close.component';
import { ArrowComponent } from './arrow/arrow.component';
import { PinComponent } from './pin/pin.component';
import { PopupComponent } from './popup/popup.component';
import { ZoomInComponent } from './zoom-in/zoom-in.component';
import { ZoomOutComponent } from './zoom-out/zoom-out.component';
import { PrevComponent } from './prev/prev.component';
import { NextComponent } from './next/next.component';
import { InfoComponent } from './info/info.component';
import { HelpComponent } from './help/help.component';
import { IconDocumentComponent } from './icon-document/icon-document.component';

@NgModule({
  declarations: [
    IconDropdownComponent,
    IconCheckboxComponent,
    CloseComponent,
    ArrowComponent,
    PinComponent,
    PopupComponent,
    ZoomInComponent,
    ZoomOutComponent,
    PrevComponent,
    NextComponent,
    InfoComponent,
    HelpComponent,
    IconDocumentComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    IconDropdownComponent,
    IconCheckboxComponent,
    CloseComponent,
    ArrowComponent,
    PinComponent,
    PopupComponent,
    ZoomInComponent,
    ZoomOutComponent,
    PrevComponent,
    NextComponent,
    InfoComponent,
    HelpComponent,
    IconDocumentComponent
  ]
})
export class IconsModule { }
