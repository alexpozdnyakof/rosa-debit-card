import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-icon-info',
  template: '<svg width="18px" height="18px" viewBox="0 0 18 18" version="1.1" fill="none"><rect id="Rectangle" stroke="#C4C4C4" x="0" y="0" width="16" height="16" rx="2"></rect><rect id="Rectangle" fill="#807D7D" fill-rule="nonzero" x="7" y="3" width="2" height="6.35231"></rect><rect id="Rectangle" fill="#807D7D" fill-rule="nonzero" x="7" y="11" width="2" height="2"></rect></svg>',
  styles: [':host{width: 16px; height: 16px;}'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoComponent {

  constructor() { }


}