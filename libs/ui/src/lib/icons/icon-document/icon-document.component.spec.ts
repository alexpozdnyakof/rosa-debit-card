import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IconDocumentComponent } from './icon-document.component';

describe('IconDocumentComponent', () => {
  let component: IconDocumentComponent;
  let fixture: ComponentFixture<IconDocumentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IconDocumentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IconDocumentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
