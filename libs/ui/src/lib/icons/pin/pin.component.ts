import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-icon-pin',
  template: '<svg width="9" height="12" viewBox="0 0 9 12" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M4.5 12S9 8 9 4.5a4.501 4.501 0 0 0-9 0C0 8 4.5 12 4.5 12zm0-6a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3z" fill="#282423"/></svg>',
  styles: [':host{width:9px; height:12px;}'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PinComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
