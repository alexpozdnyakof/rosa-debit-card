import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-icon-popup',
  template: '<svg width="28" height="28" viewBox="0 0 28 28" fill="none" xmlns="http://www.w3.org/2000/svg"><circle cx="14" cy="14" r="12" fill="#E60028"/><path d="M13.4297 19.6934L13.4297 22.001" stroke="white" stroke-width="2" stroke-miterlimit="10"/><path d="M10 10.4614V9.30759C10 7.94939 10.7982 7 12.1333 7H15.8667C17.2018 7 18 8.12199 18 9.48019L18 11.0383C18 12.3965 17.2018 13.355 15.8667 13.355C14.5315 13.355 13.4286 14.2953 13.4286 15.6535V17.3842" stroke="white" stroke-width="2" stroke-miterlimit="10"/></svg>',
  styles: [':host{width:28px;height:28px;}'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PopupComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
