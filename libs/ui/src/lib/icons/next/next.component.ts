import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-icon-next',
  template: '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24"><path d="M 8.59 16.59 L 13.17 12 L 8.59 7.41 L 10 6 L 16 12 L 10 18 Z" fill="#e60028"></path><path d="M 0 0 L 24 0 L 24 24 L 0 24 Z" fill="transparent"></path></svg>',
  styles: [':host{width: 24px; height: 24px;}'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NextComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
