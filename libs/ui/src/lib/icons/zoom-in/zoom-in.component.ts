import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-icon-zoom-in',
  template: '<svg width="20" height="21" fill="zone"><path fill-rule="evenodd" clip-rule="evenodd" d="M11 9.438v-9H9v9H0v2h9v9h2v-9h9v-2h-9z" fill="#E60028"/></svg>',
  styles: [`:host{height: 21px; width: 20px; display: block;}`],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ZoomInComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
