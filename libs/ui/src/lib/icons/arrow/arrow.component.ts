import {
  Component,
  OnInit,
  ChangeDetectionStrategy,
  Input,
  Renderer2,
  ViewChild,
  ElementRef
} from '@angular/core';

@Component({
  selector: 'rosa-icon-arrow',
  template:
    '<svg width="24px" height="20px" viewBox="0 0 24 20"><polygon id="Path" points="12.6102 1.42857 20.1356 8.97959 0 8.97959 0 11.0204 20.1356 11.0204 12.6102 18.5714 14.0339 20 24 10 14.0339 0" fill="#E60028" #icon></polygon></svg>',
  styles: [':host{width: 24px; height: 20px;}'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ArrowComponent implements OnInit {
  @ViewChild('icon', { static: true })
  iconElement: ElementRef;
  @Input() public fill = '#e60028';
  constructor(private renderer: Renderer2) {}

  ngOnInit() {
    this.renderer.setAttribute(
      this.iconElement.nativeElement,
      'fill',
      this.fill
    );
  }
}
