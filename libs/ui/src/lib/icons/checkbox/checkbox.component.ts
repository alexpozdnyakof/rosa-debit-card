import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'rosa-icon-checkbox',
  template: '<svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" clip-rule="evenodd" d="M21.744 5.67L10.23 18.44l-6.926-6.722 1.393-1.435 5.437 5.277L20.258 4.33l1.486 1.34z" fill="#E60028"/> </svg>',
  styles: [':host{width:24px;height:24px;}']
})
export class IconCheckboxComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
