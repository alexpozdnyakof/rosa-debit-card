import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-icon-close',
  template: '<svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.45463 0L16 14.5455L14.5455 16L9.57571e-05 1.45455L1.45463 0Z" fill="#282423"/><path d="M0 14.5455L14.5454 2.45218e-07L15.9999 1.45455L1.45454 16L0 14.5455Z" fill="#282423"/></svg>',
  styles: [':host{width:16px;height:16px;}'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CloseComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
