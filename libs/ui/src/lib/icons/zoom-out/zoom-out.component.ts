import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-icon-zoom-out',
  template: `<svg width="20" height="21" fill="none"><path d="M0 9.438h20v2H0v-2z" fill="#E60028"/></svg>`,
  styles: [`:host{height: 21px; width: 20px; display: block;}`],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ZoomOutComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
