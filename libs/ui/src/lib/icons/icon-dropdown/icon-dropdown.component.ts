import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'rosa-icon-dropdown',
  template: '<svg width="20" height="11" viewBox="0 0 20 11" fill="none"><path d="M20 1.375L10 11L0 1.375L1.42857 0L10 8.25L18.5714 0L20 1.375Z" fill="#282423"/></svg>',
  styles: [':host{width: 20px; height: 11px;}']
})
export class IconDropdownComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
