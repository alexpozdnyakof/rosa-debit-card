import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StepperContainerComponent } from './stepper-container/stepper-container.component';
import { StepComponent } from './step/step.component';
import { StepProgressComponent } from './step-progress/step-progress.component';



@NgModule({
  declarations: [
    StepperContainerComponent,
    StepComponent,
    StepProgressComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    StepperContainerComponent,
    StepComponent,
    StepProgressComponent
  ],
})
export class StepperModule { }
