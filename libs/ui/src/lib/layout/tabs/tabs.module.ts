import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UnderlineComponent } from './underline/underline.component';
import { TabContainerComponent } from './tab-container/tab-container.component';
import { TabComponent } from './tab/tab.component';



@NgModule({
  declarations: [
    TabContainerComponent,
    UnderlineComponent,
    TabComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    TabContainerComponent,
    UnderlineComponent,
    TabComponent
  ]
})
export class TabsModule { }
