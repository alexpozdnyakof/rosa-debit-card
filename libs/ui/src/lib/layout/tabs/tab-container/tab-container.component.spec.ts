import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabContainerComponent } from './tab-container.component';
import { Component, ViewChild, ContentChild } from '@angular/core';
import { TabComponent } from '../tab/tab.component';
import { UnderlineComponent } from '../underline/underline.component';
import { By } from '@angular/platform-browser';


@Component({
  template: `
  <rosa-tab-container  [active]="'first'">
    <rosa-tab [value]="'first'" #child>Первый тариф</rosa-tab>
    <rosa-tab [value]="'second'">Второй тариф</rosa-tab>
    <rosa-tab [value]="'third'">Третий тариф</rosa-tab>
    <rosa-underline></rosa-underline>
  </rosa-tab-container>
  `,
})
class SimpleTest {
}

describe('TabContainerComponent', () => {
  // let component: TabContainerComponent;
  let fixture: ComponentFixture<SimpleTest>;
  let containerDE, container, containerEl, tab;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabContainerComponent, TabComponent, UnderlineComponent, SimpleTest ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleTest);
    tab = fixture.debugElement.query(By.directive(TabComponent));
    containerDE = fixture.debugElement.query(By.directive(TabContainerComponent));
    container = containerDE.componentInstance;
    containerEl = containerDE.nativeElement;
    fixture.detectChanges();
  });

  it('projects the tab inside of the wrapper', () => {
    expect(containerEl.querySelector('rosa-tab')).toBeTruthy();
  });

  it('projects the all tabs inside of the wrapper', () => {
    expect(containerEl.querySelectorAll('rosa-tab').length).toBe(3);
  });

  it('projects should have underline', () => {
    expect(containerEl.querySelector('rosa-underline')).toBeTruthy();
  });

  it('should set active value', () => {
    expect(container.active).toBe('first');
  });

  it('should set first tab active', () => {
    const tabs = container.tabs.map((tabInContainer: TabComponent) => tabInContainer);
    expect(tabs[0].value).toBe(container.active);
    expect(tabs[0].active).toBeTruthy();
  });

  it('should set first tab active when container value not setted', () => {
    container.active = null;
    fixture.detectChanges();
    const tabActive = container.tabs.map((tabInContainer: TabComponent) => tabInContainer);
    expect(tabActive[0].active).toBeTruthy();
  });

  it('should set new value when clicking', () => {
    const tabs = container.tabs.map((tabInContainer: TabComponent) => tabInContainer);
    const tabsEl = containerEl.querySelectorAll('rosa-tab');
    tabsEl[1].click();
    fixture.detectChanges();
    expect(container.active).toBe(tabs[1].value);
    expect(tabs[1].active).toBeTruthy();
  });

  it('implements ngOnDestroy', () => {
    expect(container.ngOnDestroy).toBeDefined();
  });

});
