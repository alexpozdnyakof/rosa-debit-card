
import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { TabComponent } from '../tab/tab.component';
import { UnderlineComponent } from '../underline/underline.component';
import { TabContainerComponent } from './tab-container.component';
storiesOf('Tabs', module)
  .addDecorator(
    moduleMetadata({
      declarations: [
        TabContainerComponent,
        TabComponent,
        UnderlineComponent
      ],
      imports: [
        CommonModule,
      ],
    })
  )
  .add('default', () => {
    return {
      template: `
      <section class="rosa_form-group">
        <div style="width: 100%; height: 62px; background-color: #282423; padding: 0 16px;">
            <rosa-tab-container [active]="'first'">
                <rosa-tab [value]="'first'">Первый тариф</rosa-tab>
                <rosa-tab [value]="'second'">Второй тариф</rosa-tab>
                <rosa-tab [value]="'third'">Третий тариф</rosa-tab>
                <rosa-underline></rosa-underline>
            </rosa-tab-container>
        </div>
      </section>
      `,
    };
  })
