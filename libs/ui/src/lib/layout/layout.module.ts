import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsModule } from './tabs/tabs.module';
import { StepperModule } from './stepper/stepper.module';



@NgModule({
  declarations: [],
  imports: [
    TabsModule,
    StepperModule,
    CommonModule
  ],
  exports:[
    TabsModule,
    StepperModule,
  ]
})
export class LayoutModule { }
