import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: 'input[rosaCode]'
})
export class CodeInputDirective {

  constructor() { }
  @HostBinding('class.rosa-code_input')
  get medium() {
    return true;
  }

}
