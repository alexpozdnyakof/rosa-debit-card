import { Component, OnInit, AfterViewInit, OnDestroy, ContentChild } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subject, Observable, BehaviorSubject } from 'rxjs';
import { debounceTime, filter } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';

export interface ControlData {
  value: string,
  isValid: boolean
}
@Component({
  selector: 'rosa-code-container',
  templateUrl: './code-container.component.html',
  styleUrls: ['./code-container.component.scss'],
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    '[class.rosa-form-control]': 'true',
    '[class.rosa-form-control-disabled]': 'control?.disabled',
  }
})
export class CodeContainerComponent implements OnInit, AfterViewInit, OnDestroy {
  public controlValid = true;
  public controlData$: BehaviorSubject<ControlData> = new BehaviorSubject({
    value: '',
    isValid: false
  });

  @ContentChild(NgControl, { static: false })
  control: NgControl;
  private _controlChanges: Subject<NgControl> = new Subject<NgControl>();
  get controlChanges(): Observable<NgControl> {
    return this._controlChanges.asObservable();
  }
  setControl(control: NgControl) {
    this._controlChanges.next(control);
  }
  ngAfterViewInit() {
    this.control.valueChanges.pipe(
      untilDestroyed(this),
      debounceTime(300),
      filter((value: string) => value.length === 4)
      ).subscribe(
        controlValue => {
          this.isControlValid({
            value: controlValue,
            isValid: this.control.valid
          });
        }
      )
      this.control.statusChanges.pipe(
        untilDestroyed(this),
        debounceTime(300),
        ).subscribe(
          controlStatus => {
            this.controlValid = controlStatus === 'VALID' ? true : false;
          }
        )
  }
  constructor() { }

  ngOnInit() {
  }
  public controlData$$(): Observable<ControlData> {
    return this.controlData$.asObservable();
  }
  ngOnDestroy() {
    this.controlData$.next({
      value: '',
      isValid: false
    });
  }
  public isControlValid(control){
    //this.controlValid = control.isValid;
    this.controlData$.next(control);
  }
  public controlClass(){
    return 'class';
  }

}
