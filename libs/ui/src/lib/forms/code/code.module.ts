import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxMaskModule } from 'ngx-mask';
import { CodeContainerComponent } from './components/code-container/code-container.component';
import { CodeInputDirective } from './components/code-input.directive'



@NgModule({
  declarations: [
    CodeContainerComponent,
    CodeInputDirective
  ],
  imports: [
    CommonModule,
    NgxMaskModule.forRoot()
  ],
  exports: [
    CodeContainerComponent,
    CodeInputDirective
  ]
})
export class CodeModule { }
