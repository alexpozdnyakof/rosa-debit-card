import { Directive, Input } from '@angular/core';
import { NgControl } from '@angular/forms';

@Directive({
  selector: '[readOnlyControl]'
})
export class ReadonlyDirective {

  @Input() set readOnlyControl( condition : boolean ) {
    const action = condition ? 'readonly' : 'enable';
    this.ngControl.control[action]();
  }

  constructor( private ngControl : NgControl ){}
}
