import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SelectComponent } from './components/select/select.component';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { ReactiveFormsModule } from '@angular/forms';
import { SelectDirective } from './components/select.directive';
import { SelectContainerComponent } from './components/select-container/select-container.component';
import { IconsModule } from '../../icons/icons.module';

@NgModule({
  declarations: [
    SelectComponent,
    SelectDirective,
    SelectContainerComponent
  ],
  imports: [
    CommonModule,
    ScrollDispatchModule,
    ReactiveFormsModule,
    IconsModule
  ],
  exports: [
    SelectComponent,
    SelectDirective,
    SelectContainerComponent
  ]
})
export class SelectModule { }
