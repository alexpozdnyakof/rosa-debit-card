import { NgModule } from '@angular/core';
import { RosaInputDirective } from './rosa-input.directive';
import { CommonModule } from '@angular/common';
import { ContainerComponent } from './components/input-container/input-container.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    ContainerComponent,
    RosaInputDirective
  ],
  exports: [
    ContainerComponent,
    RosaInputDirective
  ]
})
export class InputModule { }
