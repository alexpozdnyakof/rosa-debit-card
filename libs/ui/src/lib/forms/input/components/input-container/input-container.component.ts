import { Component, OnInit, OnDestroy, ContentChild, AfterViewInit } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'rosa-input-container',
  templateUrl: './input-container.component.html',
  styleUrls: ['./input-container.component.scss'],
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    '[class.rosa-form-control]': 'true',
    '[class.rosa-form-control-disabled]': 'control?.disabled',
  }
})
export class ContainerComponent implements OnInit, AfterViewInit, OnDestroy {
  public controlValid = true;
  @ContentChild(NgControl, { static: false })
  control: NgControl;
  private _controlChanges: Subject<NgControl> = new Subject<NgControl>();
  get controlChanges(): Observable<NgControl> {
    return this._controlChanges.asObservable();
  }
  setControl(control: NgControl) {
    this._controlChanges.next(control);
  }
  constructor() { }
  ngAfterViewInit() {
    this.control.statusChanges.pipe(
      debounceTime(300),
      untilDestroyed(this)
      ).subscribe(
        data => {
          this.isControlValid(this.control.valid);
        }
      )
  }
  ngOnInit() {}
  ngOnDestroy() {}
  public isControlValid(isValid: boolean){
    this.controlValid = isValid;
  }
  public controlClass(){
    return 'class';
  }

}
