import { Directive, Input, HostBinding, Renderer2, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: 'input[rosaInput], textarea[rosaInput]'
})
export class RosaInputDirective implements OnInit {
  private _type: string;
  @Input('rosaInput')
  set setType(value: string) {
    this._type = value || `default`;
  }

  constructor(
    private el: ElementRef,
    private renderer: Renderer2
  ) { }
  ngOnInit(){
    switch(this._type){
      case `default`:
          this.renderer.addClass(this.el.nativeElement, 'rosa-input');
          break;
      case `large`:
          this.renderer.addClass(this.el.nativeElement, 'rosa-input_large');
          break;
      case `numeric`:
          this.renderer.addClass(this.el.nativeElement, 'rosa-input_numeric');
          break;
   }
  }

}