import { Component, OnInit, OnDestroy, ContentChild, AfterViewInit, Input, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'rosa-checkbox-container',
  templateUrl: './checkbox-container.component.html',
  styleUrls: ['./checkbox-container.component.scss'],
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    '[class.rosa-form-control]': 'true',
    '[class.rosa-form-control-disabled]': 'control?.disabled',
  }
})
export class CheckboxContainerComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() theme: 'default' | 'dark' | 'gray' = 'default';
  @ViewChild('themeContainer', {static: true})
  themeContainer: ElementRef
  public controlValid = true;
  @ContentChild(NgControl, { static: true })
  control: NgControl;
  @ContentChild('label', { static: true })
  label: HTMLLabelElement;
  private _controlChanges: Subject<NgControl> = new Subject<NgControl>();
  get controlChanges(): Observable<NgControl> {
    return this._controlChanges.asObservable();
  }
  setControl(control: NgControl) {
    this._controlChanges.next(control);
  }
  constructor(
    private renderer: Renderer2
  ) { }
  ngAfterViewInit() {
    this.control.statusChanges.pipe(
      debounceTime(300),
      untilDestroyed(this)
      ).subscribe(
        data => {
          this.isControlValid(this.control.valid);
        }
      )
  }
  ngOnInit() {
    this.setTheme();
  }
  ngOnDestroy() {}
  public isControlValid(isValid: boolean){
    this.controlValid = isValid;
  }
  public controlClass(){
    return 'class';
  }

  private setTheme(){
    switch(this.theme){
      case `default`:
          this.renderer.addClass(this.themeContainer.nativeElement, 'theme-default');
          break;
      case `dark`:
          this.renderer.addClass(this.themeContainer.nativeElement, 'theme-dark');
          break;
      case `gray`:
          this.renderer.addClass(this.themeContainer.nativeElement, 'theme-gray');
          break;
   }
  }

}
