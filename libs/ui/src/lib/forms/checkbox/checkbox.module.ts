import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CheckboxContainerComponent } from './components/checkbox-container/checkbox-container.component';
import { CheckboxDirective } from './components/checkbox.directive';
import { IconsModule } from '../../icons/icons.module';
import { CheckboxLabelDirective } from './components/checkbox-label.directive';

@NgModule({
  declarations: [
    CheckboxContainerComponent,
    CheckboxDirective,
    CheckboxLabelDirective
  ],
  imports: [
    CommonModule,
    IconsModule
  ],
  exports: [
    CheckboxContainerComponent,
    CheckboxDirective,
    CheckboxLabelDirective
  ],
})
export class CheckboxModule { }
