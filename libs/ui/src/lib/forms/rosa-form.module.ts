import { NgModule } from '@angular/core';
import { AutocompleteModule } from './autocomplete/autocomplete.module';
import { InputModule } from './input/input.module';
import { SelectModule } from './select/select.module';
import { PhoneModule } from './phone/phone.module';
import { RadioModule } from './radio/radio.module';
import { CheckboxModule } from './checkbox/checkbox.module';
import { CodeModule } from './code/code.module';
import { DisableDirective } from './disable/disable.directive';
import { ReadonlyDirective } from './readonly/readonly.directive';

@NgModule({
  declarations: [DisableDirective, ReadonlyDirective],
  imports: [
    AutocompleteModule,
    InputModule,
    SelectModule,
    PhoneModule,
    RadioModule,
    CheckboxModule,
    CodeModule
  ],
  exports: [
    AutocompleteModule,
    CheckboxModule,
    InputModule,
    PhoneModule,
    RadioModule,
    SelectModule,
    CodeModule,
    DisableDirective,
    ReadonlyDirective

  ]
})
export class RosaFormModule {}
