import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'phoneMask'
})
export class PhoneMaskPipe implements PipeTransform {

  transform(phone: any): any {
    if(!phone) {return};
    return phone.replace(/(\d{3})(\d{3})(\d{2})(\d{2})/,"$1 $2 $3 $4")
  }

}
