import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, forwardRef, OnDestroy, Renderer2 } from '@angular/core';
import { NG_VALUE_ACCESSOR, ControlValueAccessor } from '@angular/forms';

export const CUSTOM_PHONE_VALUE_ACCESSOR : any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => PhoneComponent),
  multi: true,
};

@Component({
  selector: 'rosa-phone',
  templateUrl: './phone.component.html',
  styleUrls: ['./phone.component.scss'],
  providers: [CUSTOM_PHONE_VALUE_ACCESSOR]
})
export class PhoneComponent implements OnInit, AfterViewInit, ControlValueAccessor, OnDestroy {
  @ViewChild('phoneInput', {static: true, read: ElementRef})
  phoneInput;
  public controlValue: string;
  constructor(
    public elRef: ElementRef,
    public renderer: Renderer2
  ) { }

  ngAfterViewInit(){
    //this.phoneInput.nativeElement.value = this.makeMask(`9676125383`);
    console.log(this.phoneInput.nativeElement.value);
  }
  ngOnInit() {
    //console.log(this.makeMask(`9676125383`)); //12-343-213
  }
  ngOnDestroy() {}
  onChange: any = () => {};
  onTouched: any = () => {};

  public writeValue( value : any ) : void {
    if(!value || typeof value !== 'string' ){
      return;
    }
    const phoneControl = this.phoneInput.nativeElement;
    //const valueMasked = this.makeMask(value);
    //this.controlValue = valueMasked;
    this.renderer.setProperty(phoneControl, value, value);
    //this.onChange(value);
  }
  registerOnTouched( fn : any ) : void {
    this.onTouched = fn;
  }
  registerOnChange( fn : any ) : void {
    this.onChange = fn;
  }
  change( $event ) {
    console.log($event.target.value);
    //const valueMask = this.makeMask($event.target.textContent);
    //const phoneControl = this.phoneInput.nativeElement;
    //this.renderer.setProperty(phoneControl, 'textContent', valueMask);
    //console.log($event.target.value);
    this.onChange($event.target.value);
    this.onTouched($event.target.value);
  }
  private makeMask(value: string){
    return value.replace(/(\d{3})(\d{3})(\d{2})(\d{2})/,"$1 $2 $3 $4"); //913 041 77 266

  }

}
