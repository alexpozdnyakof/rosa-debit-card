import { Directive, HostBinding } from '@angular/core';

@Directive({
  selector: 'input[rosaPhone]'
})
export class PhoneDirective {

  @HostBinding('class.rosa-phone')
  get medium() {
    return true;
  }
  constructor() { }

}
