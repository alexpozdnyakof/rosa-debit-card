import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhoneComponent } from './components/phone/phone.component';
import { PhoneMaskPipe } from './components/phone-mask.pipe';
import { NgxMaskModule } from 'ngx-mask'
import { PhoneDirective } from './components/rosa-phone.directive';
import { PhoneContainerComponent } from './components/phone-container/phone-container.component';
//export const options: Partial<IConfig> | (() => Partial<IConfig>);

@NgModule({
  declarations: [
    PhoneComponent,
    PhoneMaskPipe,
    PhoneDirective,
    PhoneContainerComponent
  ],
  imports: [
    CommonModule,
    NgxMaskModule.forRoot()
  ],
  exports: [
    PhoneComponent,
    PhoneDirective,
    PhoneContainerComponent
  ],
})
export class PhoneModule { }
