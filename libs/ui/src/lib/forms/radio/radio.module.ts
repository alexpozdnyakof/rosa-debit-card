import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RadioDirective } from './components/radio.directive';
import { RadioContainerComponent } from './components/radio-container/radio-container.component';

@NgModule({
  declarations: [
    RadioDirective,
    RadioContainerComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    RadioDirective,
    RadioContainerComponent
  ],

})
export class RadioModule { }
