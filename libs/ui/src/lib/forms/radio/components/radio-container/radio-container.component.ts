import {
  Component,
  OnInit,
  OnDestroy,
  ContentChild,
  AfterViewInit
} from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Component({
  selector: 'rosa-radio-container',
  templateUrl: './radio-container.component.html',
  styleUrls: ['./radio-container.component.scss'],
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    '[class.rosa-form-control]': 'true',
    '[class.rosa-form-control-disabled]': 'control?.disabled'
  }
})
export class RadioContainerComponent
  implements OnInit, AfterViewInit, OnDestroy {
  public controlValid = true;
  @ContentChild(NgControl, { static: true })
  control: NgControl;
  @ContentChild('label', { static: true })
  label;

  constructor() {}
  ngAfterViewInit() {}
  ngOnInit() {}
  ngOnDestroy() {}
  public isControlValid(isValid: boolean) {
    this.controlValid = isValid;
  }
  public controlClass() {
    return 'class';
  }
}
