import { Component, OnInit, OnDestroy, ContentChild, AfterViewInit, Renderer2, ViewChild, ElementRef } from '@angular/core';
import { NgControl } from '@angular/forms';
import { Subject, Observable } from 'rxjs';
import { debounceTime } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { AutocompleteComponent } from '../autocomplete/autocomplete.component';
import { AutocompleteDirective } from '../autocomplete.directive';

@Component({
  selector: 'rosa-autocomplete-container',
  templateUrl: './autocomplete-container.component.html',
  styleUrls: ['./autocomplete-container.component.scss'],
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    '[class.rosa-form-control]': 'true',
    '[class.rosa-form-control-disabled]': 'control?.disabled',
  }
})
export class AutocompleteContainerComponent implements OnInit, AfterViewInit, OnDestroy {
  public controlValid = true;
  public isOpen = false;
  @ContentChild(NgControl, { static: true })
  control: NgControl;
  @ContentChild(AutocompleteDirective, { static: false })
  autocomplete: AutocompleteDirective;
  @ViewChild('container',{static: true,  read: ElementRef} )
  container: ElementRef;
  private _controlChanges: Subject<NgControl> = new Subject<NgControl>();
  get controlChanges(): Observable<NgControl> {
    return this._controlChanges.asObservable();
  }
  setControl(control: NgControl) {
    this._controlChanges.next(control);
  }

  constructor(
    private renderer: Renderer2,
  ) { }
  ngAfterViewInit() {
    this.autocomplete.isOpen$
    .subscribe(
      (isOpen: boolean) => {
        isOpen ? this.renderer.addClass(this.container.nativeElement,  'rosa-autocomplete-container_open') :this.renderer.removeClass(this.container.nativeElement,  'rosa-autocomplete-container_open')
      }
    )
    this.control.statusChanges.pipe(
      debounceTime(300),
      untilDestroyed(this)
      ).subscribe(
        data => {
          this.isControlValid(this.control.valid);
        }
      )

  }
  ngOnInit() {}
  ngOnDestroy() {}
  public isControlValid(isValid: boolean){
    this.controlValid = isValid;
  }
  public controlClass(){
    return 'class';
  }

}
