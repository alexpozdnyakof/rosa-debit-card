import { Directive, TemplateRef } from '@angular/core';

@Directive({
  selector: '[rosaAutocompleteContent]'
})
export class AutocompleteContentDirective {
  constructor(public tpl: TemplateRef<any>) {}
}
