import {
  Directive,
  OnInit,
  ViewContainerRef,
  ElementRef,
  Input,
  OnDestroy,
  EventEmitter,
  Output,
  HostListener,
  AfterViewInit
} from '@angular/core';
import { NgControl } from '@angular/forms';
import { AutocompleteComponent } from './autocomplete/autocomplete.component';
import {
  Overlay,
  OverlayRef,
  ConnectionPositionPair
} from '@angular/cdk/overlay';
import { BehaviorSubject, fromEvent, merge } from 'rxjs';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { filter, takeUntil} from 'rxjs/operators';
import { TemplatePortal } from '@angular/cdk/portal';

@Directive({
  selector: '[rosaAutocomplete]'
})
export class AutocompleteDirective implements OnInit, AfterViewInit, OnDestroy {
  @Input() rosaAutocomplete: AutocompleteComponent;
  private overlayRef: OverlayRef = null;
  public isOpen$ = new BehaviorSubject<Boolean>(false);
  private _first: any;
  @Output()
  public valueSetted: EventEmitter<any> = new EventEmitter();
  constructor(
    private host: ElementRef<HTMLInputElement>,
    private ngControl: NgControl,
    private vcr: ViewContainerRef,
    private overlay: Overlay
  ) {}

  @HostListener('keydown', ['$event'])
  manage(event) {
    const allowedCodes = [13, 38, 40];
    if(allowedCodes.includes(event.keyCode)) {
      this.rosaAutocomplete.onKeyDown(event);
    }
  }


  get control() {
    return this.ngControl.control;
  }
 ngAfterViewInit() {}

 setValueAndClose(value){
   if(!value) return;
   this.valueSetted.emit(value)
   this.control.setValue(value.value);
   return this.close();
 }
  ngOnInit() {

    // handle input enter
    merge(
      fromEvent(this.origin, 'focus'),
      fromEvent(this.origin, 'input')
    ).pipe(untilDestroyed(this))
     .subscribe(() => {
        if(this.overlayRef == null){
          this.openDropdown();
          this.isOpen$.next(true);
        }
        this.overlayRef.detachments().pipe(untilDestroyed(this)).subscribe(
          data => {this.close()}
        )
        // handle mouse click on options
        this.rosaAutocomplete.optionsClick()
        .pipe(takeUntil(this.overlayRef.detachments()))
        .subscribe((value: any) => {this.setValueAndClose(value)});

        // handle keyboard actions like arrow nav and click enter to select
        this.rosaAutocomplete.keyBoardValue$()
        .pipe(takeUntil(this.overlayRef.detachments()))
        .subscribe((value: any) => {this.setValueAndClose(value)});
        // get first actual value to set it after blur
        this.rosaAutocomplete.optionsValues()
        .pipe(takeUntil(this.overlayRef.detachments()))
        .subscribe((options: any) => {
          this._first = options[0]})
      });
  }

  openDropdown() {
    this.overlayRef = this.overlay.create({
      width: this.origin.offsetWidth,
      maxHeight: 300,
      backdropClass: '',
      scrollStrategy: this.overlay.scrollStrategies.close(),
      positionStrategy: this.getOverlayPosition()
    });
    const template = new TemplatePortal(
      this.rosaAutocomplete.rootTemplate,
      this.vcr
    );
    this.overlayRef.attach(template);

    // handle outside click
    overlayClickOutside(this.overlayRef, this.origin).subscribe(() =>
      {
        if(this._first){
          this.valueSetted.emit(this._first)
          this.control.setValue(this._first.value);
        }
        this.close()
      }
    );
  }

  ngOnDestroy() {}

  private close() {
    if(this.overlayRef !== null) {
      this.overlayRef.detach();
      this.overlayRef = null;
      this.isOpen$.next(false);
    }
  }

  private getOverlayPosition() {
    const positions = [
      new ConnectionPositionPair(
        { originX: 'start', originY: 'bottom' },
        { overlayX: 'start', overlayY: 'top' }
      )
    ];

    return this.overlay
      .position()
      .flexibleConnectedTo(this.origin)
      .withPositions(positions)
      .withFlexibleDimensions(false)
      .withPush(false)
  }

  get origin() {
    return this.host.nativeElement;
  }
}
// outside click handler declaration
export function overlayClickOutside(
  overlayRef: OverlayRef,
  origin: HTMLElement
) {
  return fromEvent<MouseEvent>(document, 'click').pipe(
    filter(event => {
      const clickTarget = event.target as HTMLElement;
      const notOrigin = clickTarget !== origin; // the input
      const notOverlay =
        !!overlayRef &&
        overlayRef.overlayElement.contains(clickTarget) === false; // the autocomplete
      return notOrigin && notOverlay;
    }),
    takeUntil(overlayRef.detachments())
  );
}
