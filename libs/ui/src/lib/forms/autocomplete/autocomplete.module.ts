import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OptionComponent } from './components/option/option.component';
import { AutocompleteComponent } from './components/autocomplete/autocomplete.component';
import { AutocompleteDirective } from './components/autocomplete.directive';
import { SharedModule } from '../../shared/shared.module';
import { AutocompleteContentDirective } from './components/autocomplte-content.directive';
import { AutocompleteContainerComponent } from './components/autocomplete-container/autocomplete-container.component';

@NgModule({
  declarations: [
    OptionComponent,
    AutocompleteComponent,
    AutocompleteDirective,
    AutocompleteContentDirective,
    AutocompleteContainerComponent
  ],
  imports: [CommonModule, SharedModule],
  exports: [
    OptionComponent,
    AutocompleteComponent,
    AutocompleteDirective,
    AutocompleteContentDirective,
    AutocompleteContainerComponent
  ]
})
export class AutocompleteModule {}
