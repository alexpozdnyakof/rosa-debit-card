import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';

import { ControlErrorComponent } from './components/control-error/control-error.component';
import { ControlHelperComponent } from './components/control-helper/control-helper.component';
import { FilterPipe } from './pipes/filter.pipe';
@NgModule({
  declarations: [
    ControlErrorComponent,
    ControlHelperComponent,
    FilterPipe
  ],
  imports: [CommonModule],
  exports: [
    ControlErrorComponent,
    ControlHelperComponent,
    OverlayModule,
    FilterPipe
  ]
})
export class SharedModule {}
