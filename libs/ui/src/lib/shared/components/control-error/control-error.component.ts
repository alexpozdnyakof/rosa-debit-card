import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-control-error',
  template: '<span class="rosaControlError"><ng-content></ng-content></span>',
  styles: ['.rosaControlError {font-size: 12px;line-height: 17px;color: #C12929;font-weight: 400;}'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ControlErrorComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
