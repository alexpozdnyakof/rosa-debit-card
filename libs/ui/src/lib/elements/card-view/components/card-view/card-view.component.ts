import { Component, OnInit, Input, Renderer2, ElementRef, ViewChild, AfterViewInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-card-view',
  templateUrl: './card-view.component.html',
  styleUrls: ['./card-view.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CardViewComponent implements OnInit, AfterViewInit {
  @Input()
  cardUrl: string;
  @ViewChild('cardView', {static: true})
  cardView: ElementRef;

  constructor(
    private renderer: Renderer2,
    private el: ElementRef
  ) { }
  ngAfterViewInit(): void {
    this._setImage(this.cardView, this.cardUrl )
  }
  ngOnInit() {
  }

  protected _setImage(element: ElementRef, url: string){
    return this.renderer.setAttribute(element.nativeElement, 'src', url);
  }

}
