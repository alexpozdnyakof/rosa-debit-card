import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { IconsModule } from '../../icons/icons.module';
import { DatepickerContainerComponent } from './components/datepicker-container/datepicker-container.component';
import { DatepickerDirective } from './components/datepicker.directive';

@NgModule({
  declarations: [
    DatepickerComponent,
    DatepickerContainerComponent,
    DatepickerDirective
  ],
  imports: [
    CommonModule,
    IconsModule
  ],
  exports: [
    DatepickerComponent,
    DatepickerContainerComponent,
    DatepickerDirective
  ],
})
export class DatepickerModule { }
