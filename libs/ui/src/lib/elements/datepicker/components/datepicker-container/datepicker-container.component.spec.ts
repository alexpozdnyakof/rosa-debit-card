import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DatepickerContainerComponent } from './datepicker-container.component';

xdescribe('DatepickerContainerComponent', () => {
  let component: DatepickerContainerComponent;
  let fixture: ComponentFixture<DatepickerContainerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DatepickerContainerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DatepickerContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
