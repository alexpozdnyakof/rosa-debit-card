import { Component, OnInit, AfterViewInit, OnDestroy, ContentChild, ViewChild, Renderer2, ElementRef } from '@angular/core';
import { DatepickerComponent } from '../datepicker/datepicker.component';
import { NgControl } from '@angular/forms';
import { Subject, Observable, fromEvent } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';
import { untilDestroyed } from 'ngx-take-until-destroy';
import { DatepickerDirective } from '../datepicker.directive';

@Component({
  selector: 'rosa-datepicker-container',
  templateUrl: './datepicker-container.component.html',
  styleUrls: ['./datepicker-container.component.scss'],
  // tslint:disable-next-line:no-host-metadata-property
  host: {
    '[class.rosa-form-control]': 'true',
    '[class.rosa-form-control-disabled]': 'control?.disabled',
  }
})
export class DatepickerContainerComponent implements OnInit, AfterViewInit, OnDestroy {

  public controlValid = true;
  public isOpen = false;
  @ContentChild(NgControl, { static: true })
  control: NgControl;
  @ContentChild(DatepickerComponent, {static: true} )
  datepicker: DatepickerComponent;
  @ContentChild(DatepickerDirective, {static: true} )
  rosaDatepicker: DatepickerDirective;
  @ViewChild('datepickerContainer', {static: true})
  datepickerContainer: ElementRef;

  private _controlChanges: Subject<NgControl> = new Subject<NgControl>();
  get controlChanges(): Observable<NgControl> {
    return this._controlChanges.asObservable();
  }
  setControl(control: NgControl) {
    this._controlChanges.next(control);
  }
  constructor(
    private host: ElementRef,
    private _renderer: Renderer2
  ) { }
  ngAfterViewInit(): void {
    this.overlayClickOutside().subscribe(
      data => {
        if(!this.host.nativeElement.contains(data)){
          this._closeDatepicker();
        }
      }
    )
    this.rosaDatepicker.isOpen$$().subscribe(
      isOpen => this._openDatepicker(isOpen)
    )
    this.control.valueChanges.pipe(
      debounceTime(300),
      untilDestroyed(this)
      ).subscribe(
        data => {
          this.isControlValid(this.control.valid);
        }
    );
    //this.datepicker.el.nativeElement
    // throw new Error("Method not implemented.");
  }
  ngOnInit() {

  }
  ngOnDestroy() {}
  private _openDatepicker(isOpen){
      return isOpen ?
              this._renderer.addClass(this.datepickerContainer.nativeElement, 'datepicker_open') :
              this._renderer.removeClass(this.datepickerContainer.nativeElement, 'datepicker_open')
  }
  private _closeDatepicker(){
    this.rosaDatepicker.isOpen = false;
    this._renderer.removeClass(this.datepickerContainer.nativeElement, 'datepicker_open')
  }

  public isControlValid(isValid: boolean){
    this.controlValid = isValid;
  }
  public controlClass(){
    return 'class';
  }
  overlayClickOutside() {
    return fromEvent<MouseEvent>(document, 'click')
    .pipe(
      untilDestroyed(this),
      map(event => event.target as HTMLElement)
    )
  }
}



