import {
  Component,
  OnInit,
  AfterViewInit,
  OnDestroy,
  Input
} from '@angular/core';
import {
  format,
  startOfMonth,
  endOfMonth,
  isSameMonth,
  isToday,
  isSameDay,
  getMonth,
  setMonth,
  addMonths,
  lastDayOfMonth,
  getDay,
  addDays,
  setDate,
  eachDayOfInterval,
  isWithinInterval,
  isSunday,
  isSaturday
} from 'date-fns';
import { ru } from 'date-fns/locale';
import { BehaviorSubject, Observable } from 'rxjs';

@Component({
  selector: 'rosa-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  exportAs: 'rosaDatepicker'
})
export class DatepickerComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() public availableInterval: number;
  @Input() public monthControls = true;
  public MONTH_LABELS = [
    'Январь',
    'Февраль',
    'Март',
    'Апрель',
    'Май',
    'Июнь',
    'Июль',
    'Август',
    'Сентябрь',
    'Октябрь',
    'Ноябрь',
    'Декабрь'
  ];
  public today = new Date();
  public cursor = this.today;
  public selected = this.today;
  public month = this.cursor.getMonth();
  public year = this.cursor.getFullYear();
  public monthLabel = this.MONTH_LABELS[this.month];
  private selected$: BehaviorSubject<String> = new BehaviorSubject(
    format(this.selected, 'ddMMyyyy')
  );
  public date = this.cursor;
  public startOf = startOfMonth(this.cursor);
  public endOf = endOfMonth(this.cursor);
  public days;
  @Input()
  isWeekendAvailable = {
    saturday: true,
    sunday: true
  };

  constructor() {}
  ngAfterViewInit(): void {}
  ngOnInit() {
    this.initDays();
  }
  ngOnDestroy() {}

  public isDayAvailable(day) {
    if (!this.availableInterval) return false;
    const start = addDays(this.today, -1);
    const end = addDays(this.today, this.availableInterval);
    return (
      isWithinInterval(day.date, { start: start, end: end }) &&
      this.isWeekendDayAvailable(day)
    );
  }
  private isWeekendDayAvailable(day) {
    const weekDays = [5, 6];
    if (!weekDays.includes(day.date.getDay())) {
      return true;
    }
    return (
      (day.date.getDay() === 5 && this.isWeekendAvailable.saturday) ||
      (day.date.getDay() === 6 && this.isWeekendAvailable.sunday)
    );
    /*
    console.log(`is saturday` + `${day}`.indexOf('Sat'));
    console.log(`is sunday` + `${day}`.indexOf('Sun'));
    if (`${day}`.indexOf('Sun') === -1 || `${day}`.indexOf('Sat') === -1) {
      console.log(day + 'not weekend');

      return true;
    }
    console.log(this.isWeekendAvailable);

    if (
      (`${day}`.indexOf('Sun') !== -1 && this.isWeekendAvailable.sunday) ||
      (`${day}`.indexOf('Sat') !== -1 && this.isWeekendAvailable.saturday)
    ) {
      return true;
    }
    return false;*/
  }
  public selected$$(): Observable<any> {
    return this.selected$.asObservable();
  }
  public initDays() {
    this.days = eachDayOfInterval({
      start: this.startOf,
      end: this.endOf
    }).map(day => ({
      date: day
    }));
    const currentMonthFirstDay = getDay(this.days[0].date) - 1; // получим первый день текущего месяца
    let previousMonthCursor = lastDayOfMonth(addMonths(this.cursor, -1)); // получим последний день пред. месяца

    for (let i = currentMonthFirstDay; i > 0; i--) {
      this.days.unshift({
        date: previousMonthCursor,
        isCurrentMonth: false,
        isToday: isToday(previousMonthCursor),
        isSelected: isSameDay(this.selected, previousMonthCursor)
      });
      previousMonthCursor = addDays(previousMonthCursor, -1);
    }

    const daysNeededAtEnd =
      this.days.length % 7 > 0 ? 7 - (this.days.length % 7) : 0;
    let nextMonthCursor = addMonths(this.cursor, 1);
    nextMonthCursor = setDate(nextMonthCursor, 1);
    for (let x = 1; x <= daysNeededAtEnd; x++) {
      this.days.push({
        date: nextMonthCursor,
        isCurrentMonth: false,
        isToday: isToday(nextMonthCursor),
        isSelected: isSameDay(this.selected, nextMonthCursor)
      });
      nextMonthCursor = addDays(nextMonthCursor, 1);
    }
    return this.days;
  }

  public dayClass(day) {
    return {
      day_today: isToday(day),
      day_notsame: !isSameMonth(new Date(this.year, this.month), day),
      day_selected: isSameDay(this.selected, day)
    };
  }

  public setSelected(day) {
    this.selected = day.date;
    // change calendar to correct month if they select previous or next month's days
    if (!day.isCurrentMonth) {
      const selectedMonth = getMonth(this.selected);
      this.cursor = setMonth(this.cursor, selectedMonth);
    }
    console.log(day.date);
    //DDMMYYYY
    this.selected$.next(format(this.selected, 'ddMMyyyy', { locale: ru }));
  }

  public nextMonth() {
    this.cursor = addMonths(this.cursor, 1);
    this.initDays();
  }
  public previousMonth() {
    this.cursor = addMonths(this.cursor, -1);
    this.initDays();
  }

  // gen days for next month

  // create month
  // pick when the first day
  //
}
