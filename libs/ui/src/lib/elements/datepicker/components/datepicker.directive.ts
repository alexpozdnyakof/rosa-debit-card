import { Directive, Input, ElementRef, OnInit, OnDestroy } from '@angular/core';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { BehaviorSubject, fromEvent, Observable } from 'rxjs';
import { NgControl } from '@angular/forms';
import { untilDestroyed } from 'ngx-take-until-destroy';

@Directive({
  selector: '[rosaDatepicker]'
})
export class DatepickerDirective implements OnInit, OnDestroy {
  @Input() rosaDatepicker: DatepickerComponent;
  public isOpen = false;
  public isOpen$ = new BehaviorSubject<Boolean>(false);
  constructor(
    private host: ElementRef<HTMLInputElement>,
    private ngControl: NgControl,
  ) { }
  get control() {
    return this.ngControl.control;
  }
  get origin() {
    return this.host.nativeElement;
  }
  public isOpen$$(): Observable<any>{
    return this.isOpen$.asObservable();
  }
  ngOnInit(){
    this.rosaDatepicker.selected$$().subscribe(
      value => {
        this.isOpen = false;
        this.isOpen$.next(this.isOpen);
        this.control.setValue(value);
      }
    )
    fromEvent(this.origin, 'click')
    .pipe(untilDestroyed(this))
    .subscribe(() => {
      this.isOpen$.next(this.isOpen = !this.isOpen);
    });
  }
  ngOnDestroy(){

  }
}
