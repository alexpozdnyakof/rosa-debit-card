import { NgModule } from '@angular/core';
import { CardViewModule } from './card-view/card-view.module';
import { DatepickerModule } from './datepicker/datepicker.module';
import { GrayBoxModule } from './gray-box/gray-box.module';
import { ModalModule } from './modal/modal.module';

@NgModule({
  declarations: [],
  imports: [
    CardViewModule,
    DatepickerModule,
    GrayBoxModule,
    ModalModule
  ],
  exports: [
    CardViewModule,
    DatepickerModule,
    GrayBoxModule,
    ModalModule
  ]
})
export class ElementsModule { }
