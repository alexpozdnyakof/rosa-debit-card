import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GrayBoxComponent } from './components/gray-box/gray-box.component';



@NgModule({
  declarations: [GrayBoxComponent],
  imports: [
    CommonModule
  ],
  exports: [GrayBoxComponent],
})
export class GrayBoxModule { }
