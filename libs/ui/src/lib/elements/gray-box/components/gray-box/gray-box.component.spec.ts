import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GrayBoxComponent } from './gray-box.component';

xdescribe('GrayBoxComponent', () => {
  let component: GrayBoxComponent;
  let fixture: ComponentFixture<GrayBoxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GrayBoxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GrayBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
