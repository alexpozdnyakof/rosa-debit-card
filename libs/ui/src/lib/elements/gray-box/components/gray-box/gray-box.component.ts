import { Component, OnInit, ChangeDetectionStrategy } from '@angular/core';

@Component({
  selector: 'rosa-gray-box',
  templateUrl: './gray-box.component.html',
  styleUrls: ['./gray-box.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GrayBoxComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
