import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OverlayModule } from '@angular/cdk/overlay';
import { PopupModule } from './popup/popup.module';

@NgModule({
  declarations: [

  ],
  imports: [
  CommonModule,
    OverlayModule,
    PopupModule
  ],
  exports:[
    PopupModule,
  ]
})
export class OverlaysModule { }
