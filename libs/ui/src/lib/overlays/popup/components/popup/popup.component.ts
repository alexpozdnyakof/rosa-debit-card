import { Component, OnInit, Input } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'rosa-popup',
  template: '<div class="popup"><header><rosa-icon-popup></rosa-icon-popup><rosa-icon-close (click)="closePopup()"></rosa-icon-close></header><div class="content">{{text}}</div></div>',
  styleUrls: ['./popup.component.scss']
})
export class PopupComponent implements OnInit {
  @Input() text = '';
  public closed$: BehaviorSubject<Boolean> = new BehaviorSubject(false);
  constructor() { }
  closePopup(){
    this.closed$.next(true);
  }
  ngOnInit() {
  }

}
