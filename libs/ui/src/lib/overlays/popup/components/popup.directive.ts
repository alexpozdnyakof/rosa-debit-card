import { untilDestroyed } from 'ngx-take-until-destroy';
import { Directive, Input, HostListener, ComponentRef, OnInit, ElementRef, Inject, OnDestroy } from '@angular/core';
import { Overlay, OverlayRef, OverlayPositionBuilder, NoopScrollStrategy, OverlayConfig } from '@angular/cdk/overlay';
import { ComponentPortal } from '@angular/cdk/portal';
import { PopupComponent } from './popup/popup.component';
import { fromEvent } from 'rxjs';
import { debounceTime, map, filter, tap } from 'rxjs/operators';
declare var window: any;

export interface WindowSize {
  height: number,
  width: number
};
@Directive({
  selector: '[rosaPopup]'
})
export class PopupDirective implements OnInit, OnDestroy {
  private overlayRef: OverlayRef;
  @Input('rosaPopup') text = '';
  private _windowWidth = window.innerWidth;
  @HostListener('click')
  show(){
    if(this.overlayRef.hasAttached()) {
      return this.overlayRef.detach();
    }
    const tooltipPortal = new ComponentPortal(PopupComponent);
    const tooltipRef: ComponentRef<PopupComponent> = this.overlayRef.attach(tooltipPortal);
    tooltipRef.instance.text = this.text;
    tooltipRef.instance.closed$
    .pipe(
      untilDestroyed(this),
      filter((data: boolean) => data),
      tap(() => {this.overlayRef.detach()})
    ).subscribe()
  }

  @HostListener('mouseout')
  hide() {
   // this.overlayRef.detach();
  }
  constructor(
    private overlay: Overlay,
    private elementRef: ElementRef,
    private overlayPositionBuilder: OverlayPositionBuilder,

  ) {
    fromEvent(window, 'resize').pipe(
      untilDestroyed(this),
      debounceTime(100),
      map((event: any) => <WindowSize>{
        width: event['currentTarget'].innerWidth,
        height: event['currentTarget'].innerHeight
      })
    ).subscribe((windowSize) => {
        //this.windowSizeChanged.next(windowSize);
    })
    this.overlay.scrollStrategies.block();

  };

  ngOnInit() {

    if(this._windowWidth < 768){
      const positionStrategy = this.overlayPositionBuilder
      .global()
      .centerHorizontally()
      .centerVertically();
      this.overlayRef = this.overlay.create(this.getOverlayConfig(positionStrategy, this.overlay.scrollStrategies.block()));
    } else {
      const positionStrategy = this.overlayPositionBuilder
      .flexibleConnectedTo(this.elementRef)
      .withPositions([{
        originX: 'end',
        originY: 'top',
        overlayX: 'start',
        overlayY: 'top',
      }]);

      this.overlayRef = this.overlay.create(this.getOverlayConfig(positionStrategy, this.overlay.scrollStrategies.close()));
    }

  // Connect position strategy
  }

  private getOverlayConfig(positionStrategy, scrollStrategy): OverlayConfig {

    const overlayConfig = new OverlayConfig({
      hasBackdrop: false,
      scrollStrategy: scrollStrategy,
      positionStrategy
    });

    return overlayConfig;
  }
  ngOnDestroy(){}


}
