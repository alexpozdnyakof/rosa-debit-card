import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PopupDirective } from './components/popup.directive';
import { PopupComponent } from './components/popup/popup.component';
import { IconsModule } from '../../icons/icons.module';

@NgModule({
  declarations: [
    PopupDirective,
    PopupComponent
  ],
  imports: [
    CommonModule,
    IconsModule
  ],
  exports: [
    PopupDirective,
    PopupComponent
  ],
  entryComponents: [
    PopupComponent
  ],
})
export class PopupModule { }
