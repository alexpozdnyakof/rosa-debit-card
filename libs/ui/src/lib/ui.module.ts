import { ContentModule } from './content/content.module';
import { LayoutModule } from './layout/layout.module';
import { NgModule } from '@angular/core';
import { SharedModule } from './shared/shared.module';
import { ButtonModule } from './buttons/button.module';
import { RosaFormModule } from './forms/rosa-form.module';
import { IconsModule } from './icons/icons.module';
import { ElementsModule } from './elements/elements.module';
import { OverlaysModule } from './overlays/overlays.module';

@NgModule({
  imports: [
  IconsModule,
    RosaFormModule,
    SharedModule,
    ButtonModule,
    ElementsModule,
    OverlaysModule,
    LayoutModule,
    ContentModule
  ],
  exports: [
    SharedModule,
    ButtonModule,
    IconsModule,
    RosaFormModule,
    ElementsModule,
    OverlaysModule,
    LayoutModule,
    ContentModule
  ],
  declarations: []
})
export class UiModule {}
