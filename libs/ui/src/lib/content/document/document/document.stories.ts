
import { storiesOf, moduleMetadata } from '@storybook/angular';
import { CommonModule } from '@angular/common';
import { DocumentComponent } from './document.component';
import { DocumentIconComponent } from '../document-icon/document-icon.component';

export const doc = {
    title: 'Условия и тарифы по продукту «Дебетовая карта»',
    description: 'действительны с 20.10.2019',
    size: 'pdf, 250kb'
  };

const props = {
    document: doc,
};
storiesOf('Document', module)
  .addDecorator(
    moduleMetadata({
      declarations: [
        DocumentComponent,
        DocumentIconComponent,
      ],
      imports: [
        CommonModule,
      ],
    })
  )
  .add('default', () => {
    return {
      template: `
      <section class="rosa_form-group">
        <rosa-document [document]="document"></rosa-document>
      </section>
      `,
      props,
    };
  })
