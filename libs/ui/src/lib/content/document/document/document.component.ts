import { Component, OnInit, Input } from '@angular/core';

export interface DocSettings{
  title: string;
  description?: string;
  size?: string;
}

@Component({
  selector: 'rosa-document',
  templateUrl: './document.component.html',
  styleUrls: ['./document.component.css']
})
export class DocumentComponent implements OnInit {
  @Input() document: DocSettings = {
    title: 'Название документа'
  };
  constructor(){}
  ngOnInit() {}
}
