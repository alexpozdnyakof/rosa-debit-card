import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentComponent } from './document/document.component';
import { DocumentIconComponent } from './document-icon/document-icon.component';



@NgModule({
  declarations: [
    DocumentComponent,
    DocumentIconComponent
  ],
  imports: [
    CommonModule
  ],
  exports: [
    DocumentComponent,
    DocumentIconComponent
  ],
})
export class DocumentModule { }
