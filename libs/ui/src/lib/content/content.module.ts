import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DocumentModule } from './document/document.module';



@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    DocumentModule
  ],
  exports: [
    DocumentModule
  ]
})
export class ContentModule { }
